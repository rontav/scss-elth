var express = require('express');
var app = express();
var path = require('path');

app.use('/', express.static(path.resolve(__dirname, './toDeploy')));
app.get('*', function (request, response) {
    response.sendFile(path.resolve(__dirname, './toDeploy', 'index.html'));
});

var server = app.listen(8080, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Listening on ', host, port);
});
