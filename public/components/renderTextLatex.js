import React from 'react';
import Math from '../components/katex.js';

function renderTextLatex(text) {
    if (typeof text != 'string') {
        return text;
    }
    if (text.indexOf('$') == -1) {
        return text;
    }
    var output = {};
    var returnedElems = [];

    var blockLatexRegex = /\$\$((.|\n)+?)\$\$/igm;
    var inlineLatexRegex = /\$((.|\n)+?)\$/igm;
    var handleEscapedDollars = /\\\$/igm;

    var types = ['escape', 'block', 'inline'];
    var regexes = [handleEscapedDollars, blockLatexRegex, inlineLatexRegex];
    var lastText = text;
    var lastIndex = 0;

    for (var i = 0; i < 3; i++) {
        var escaped = regexes[i].exec(lastText);
        var newText = '';
        lastIndex = 0;

        if (!escaped) {
            newText = lastText;
        }

        while (escaped) {
            output[escaped.index] = [escaped[0], escaped[1], types[i]];

            newText = newText +
                lastText.substr(lastIndex, escaped.index - lastIndex) +
                Array(escaped[0].length + 1).join(String.fromCharCode(27));

            lastIndex = escaped.index + escaped[0].length;
            escaped = regexes[i].exec(lastText);
        }
        if (lastIndex > 0) {
            newText += lastText.substr(lastIndex);
        }
        lastText = newText;
    }

    lastIndex = 0;
    Object.keys(output).forEach(startIndex => {
        startIndex = parseInt(startIndex);
        var outputObj = output[startIndex];
        if (outputObj[2] == 'escape') {
            returnedElems.push(text.substr(lastIndex, startIndex - lastIndex) + text.substr(startIndex + 1, outputObj[0].length - 1));
            lastIndex = startIndex + outputObj[0].length;
            return;
        }
        returnedElems.push(text.substr(lastIndex, startIndex - lastIndex));

        if (outputObj[2] == 'block') {
            returnedElems.push(<Math block key={startIndex}>{outputObj[1]}</Math>);
        }
        if (outputObj[2] == 'inline') {
            returnedElems.push(<Math inline key={startIndex}>{outputObj[1]}</Math>);
        }
        lastIndex = startIndex + outputObj[0].length;
    });
    returnedElems.push(text.substr(lastIndex));

    return returnedElems;
}

export default renderTextLatex;