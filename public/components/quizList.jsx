import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Button } from 'material-react';
import moment from 'moment';

import { cloneDeep } from 'lodash';

import '../scss/quizList.scss';

class TableLine extends React.Component {
    static propTypes = {
        quiz: PropTypes.object,
        quizKey: PropTypes.any
    }
    render() {
        var quiz = this.props.quiz;

        return (
            <div className="quiz">
                <div className="title"><div>{quiz.title}{quiz.time ? ' - ' + quiz.time.format('mm:ss') : ''}</div></div>
                <div className="type"><div>{quiz.type}</div></div>
                <div className="buttons">
                    {
                        quiz.timelineID ?
                            <Link to={`/quiz/${quiz.timelineID}`}><Button flat>Start!</Button></Link>
                            :
                            <Link to={`/training/${this.props.quizKey}`}><Button flat>Start!</Button></Link>
                    }
                </div>
            </div>
        );
    }
}

class LoginPage extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        database: PropTypes.object
    }
    constructor() {
        super();
        this.state = {};
    }
    async componentDidMount() {
        var database = this.props.database;

        var quizzes =
            await database
                .ref('/quizzes/')
                .orderByChild('type')
                .equalTo('Test de pregătire')
                .once('value');

        quizzes = quizzes.val();
        Object.keys(quizzes).forEach(key => {
            if (quizzes[key].hidden) {
                delete quizzes[key];
            }
        });
        this.state.quizzes = quizzes || {};
        this.originalQuizzes = cloneDeep(this.state.quizzes);

        this.timelineRef = database
            .ref('/timeline/')
            .orderByChild('active')
            .equalTo(true);
        this.timelineRef.on('value', async (timelines) => {
            if (this.interval) {
                clearInterval(this.interval);
            }
            this.state.quizzes = cloneDeep(this.originalQuizzes);
            timelines = timelines.val() || {};
            var notMarked = [];
            Object.keys(this.state.quizzes).forEach(key => {
                if (this.state.quizzes[key].stopTime) {
                    notMarked.push(key);
                }
            });

            await Promise.all(Object.keys(timelines).map(async key => {
                var timeline = timelines[key];
                
                if (!timeline.students || timeline.students.indexOf(this.props.user.uid) == -1) {
                    return;
                }
                
                timeline.stop -= 30 * 1000;
                if (notMarked.indexOf(timeline.quiz) != -1) {
                    notMarked.slice(notMarked.indexOf(timeline.quiz), 1);
                }
                if (timeline.stop - Date.now() <= 0 || (timeline.answers && timeline.answers[this.props.user.uid])) {
                    delete this.state.quizzes[timeline.quiz];
                    return;
                }

                var quiz = await database.ref(`/quizzes/${timeline.quiz}`).once('value');

                if (quiz.hidden) {
                    delete this.state.quizzes[timeline.quiz];
                    return;
                }

                this.state.quizzes[timeline.quiz] = quiz.val();
                this.state.quizzes[timeline.quiz].timelineID = key;
                this.state.quizzes[timeline.quiz].stopTime = timeline.stop;
                this.state.quizzes[timeline.quiz].time = moment(timeline.stop - Date.now());
            }));
            notMarked.forEach(key => {
                delete this.state.quizzes[key];
            });

            this.interval = setInterval(() => {
                Object.keys(this.state.quizzes).forEach(key => {
                    var quiz = this.state.quizzes[key];
                    if (quiz.stopTime) {
                        quiz.time = moment(quiz.stopTime - Date.now());
                    }
                });
                this.forceUpdate();
            }, 1000);

            this.forceUpdate();
        });
    }
    componentWillUnmount() {
        if (this.timelineRef) {
            this.timelineRef.off();
        }
        if (this.interval) {
            clearInterval(this.interval);
        }
    }
    render() {
        if (!this.state.quizzes) {
            return (
                <div className="loading">Loading</div>
            );
        }

        return (
            <div className="quizList">
                <h1 className="quizHeader">Alege un quiz</h1>
                <div className="quiz th" key="-1">
                    <div className="title"><div>Titlul quiz-ului</div></div>
                    <div className="type"><div>Tipul quiz-ului</div></div>
                    <div className="time"><div>Timp rămas</div></div>
                    <div className="buttons"></div>
                </div>
                {
                    Object.keys(this.state.quizzes).map((quizKey, index) => <TableLine quiz={this.state.quizzes[quizKey]} quizKey={quizKey} key={index} />)
                }
            </div>
        );
    }
}

export default LoginPage;