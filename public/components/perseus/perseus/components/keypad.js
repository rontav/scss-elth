/**
 * A keypad component that acts as a container for rows or columns of buttons,
 * and manages the rendering of echo animations on top of those buttons.
 */

const React = require('react');
const ReactDOM = require('react-dom');
const { connect } = require('react-redux');

import classnames from 'classnames';

const { removeEcho } = require('../actions');

const Keypad = React.createClass({
    propTypes: {
        // Whether the keypad is active, i.e., whether it should be rendered as
        // visible or invisible.
        active: React.PropTypes.bool,
        children: React.PropTypes.oneOfType([
            React.PropTypes.arrayOf(React.PropTypes.node),
            React.PropTypes.node,
        ]),
        style: React.PropTypes.any,
        className: React.PropTypes.string
    },

    componentDidMount() {
        window.addEventListener('resize', this._onResize);
        this._updateSizeAndPosition();
    },

    componentWillReceiveProps(newProps) {
        if (!this._container && (newProps.popover || newProps.echoes.length)) {
            this._computeContainer();
        }
    },

    componentWillUnmount() {
        window.removeEventListener('resize', this._onResize);
    },

    _computeContainer() {
        const domNode = ReactDOM.findDOMNode(this);
        this._container = domNode.getBoundingClientRect();
    },

    _updateSizeAndPosition() {
        // Mark the container for recalculation next time the keypad is
        // opened.
        // TODO(charlie): Since we're not recalculating the container
        // immediately, if you were to resize the page while a popover were
        // active, you'd likely get unexpected behavior. This seems very
        // difficult to do and, as such, incredibly unlikely, but we may
        // want to reconsider the caching here.
        this._container = null;
    },

    _onResize() {
        // Whenever the page resizes, we need to recompute the container's
        // bounding box. This is the only time that the bounding box can change.

        // Throttle resize events -- taken from:
        //    https://developer.mozilla.org/en-US/docs/Web/Events/resize
        if (this._resizeTimeout == null) {
            this._resizeTimeout = setTimeout(() => {
                this._resizeTimeout = null;

                if (this.isMounted()) {
                    this._updateSizeAndPosition();
                }
            }, 66);
        }
    },

    render() {
        const {
            children,
            style,
        } = this.props;

        var classes = classnames('view keypadInner', this.props.className);

        return (
            <div className={classes} style={style}>
                {children}
            </div>
        );
    },
});

const mapStateToProps = (state) => {
    return {
        ...state.echoes,
        active: state.keypad.active,
        popover: state.gestures.popover,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        removeEcho: (animationId) => {
            dispatch(removeEcho(animationId));
        },
    };
};

module.exports = connect(mapStateToProps, mapDispatchToProps)(Keypad);
