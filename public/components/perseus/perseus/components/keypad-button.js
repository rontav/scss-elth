/**
 * A component that renders a keypad button.
 */

const React = require('react');
const { connect } = require('react-redux');

import classnames from 'classnames';
import { Button } from 'material-react';

const Icon = require('./icon');
const { KeyTypes, BorderStyles } = require('../consts');
const {
    bordersPropType,
    iconPropType
} = require('./prop-types');

class KeypadButton extends React.PureComponent {
    static propTypes = {
        borders: bordersPropType,
        heightPx: React.PropTypes.number.isRequired,
        icon: iconPropType,
        onTouchCancel: React.PropTypes.func,
        onTouchEnd: React.PropTypes.func,
        onTouchMove: React.PropTypes.func,
        onTouchStart: React.PropTypes.func,
        popoverEnabled: React.PropTypes.bool,
        style: React.PropTypes.any,
        type: React.PropTypes.oneOf(Object.keys(KeyTypes)).isRequired,
        // NOTE(charlie): We may want to make this optional for phone layouts
        // (and rely on Flexbox instead), since it might not be pixel perfect
        // with borders and such.
        widthPx: React.PropTypes.number.isRequired,
        classNames: React.PropTypes.string
    }
    static defaultProps = {
        borders: BorderStyles.ALL,
        childKeys: [],
        disabled: false,
        focused: false,
        popoverEnabled: false,
    }
    render() {
        const {
            borders,
            icon,
            onTouchCancel,
            onTouchEnd,
            onTouchMove,
            onTouchStart,
            type,
        } = this.props;

        var wrapperStyle = {
            height: this.props.heightPx,
            width: this.props.widthPx,
            maxWidth: this.props.widthPx
        };
        var buttonStyle = {
            padding: `${this.props.heightPx / 2 - 24}px ${this.props.widthPx / 2 - 24}px`
        };

        const eventHandlers = {
            onTouchCancel, onTouchEnd, onTouchMove, onTouchStart,
        };

        var classes = classnames({
            view: true,
            key: true,
            centered: true,
            empty: type === KeyTypes.EMPTY,
            borderLeft: borders.indexOf('LEFT') != -1,
            borderBottom: borders.indexOf('BOTTOM') != -1
        }, this.props.classNames);

        if (type === KeyTypes.EMPTY) {
            return <div className={classes} {...eventHandlers} style={wrapperStyle} />;
        }
        else {
            return (
                <div className={classes} {...eventHandlers} style={wrapperStyle}>
                    <Button flat style={buttonStyle}>
                        <Icon icon={icon} />
                    </Button>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return state.layout.buttonDimensions;
};

module.exports = connect(mapStateToProps)(KeypadButton);
