/**
 * A keypad with two pages of keys.
 */

const React = require('react');
const { connect } = require('react-redux');

const Keypad = require('./keypad');
const ViewPager = require('./view-pager');
const PagerIndicator = require('./pager-indicator');

const TwoPageKeypad = React.createClass({
    propTypes: {
        currentPage: React.PropTypes.oneOf([0, 1]).isRequired,
        leftPage: React.PropTypes.node.isRequired,
        paginationEnabled: React.PropTypes.bool.isRequired,
        rightPage: React.PropTypes.node.isRequired,
    },

    render() {
        const {
            currentPage,
            leftPage,
            paginationEnabled,
            rightPage,
        } = this.props;

        if (paginationEnabled) {
            return (
                <Keypad>
                    <PagerIndicator numPages={2} currentPage={currentPage} />
                    <div className="view borderTop">
                        <ViewPager>
                            {leftPage}
                            {rightPage}
                        </ViewPager>
                    </div>
                </Keypad>
            );
        }
        else {
            return (
                <Keypad>
                    <div className="view row">
                        <div className="view fullWidth">
                            {leftPage}
                        </div>
                        <div className="view fullWidth">
                            {rightPage}
                        </div>
                    </div>
                </Keypad>
            );
        }
    },
});

const mapStateToProps = (state) => {
    return {
        paginationEnabled: state.layout.paginationEnabled,
    };
};

module.exports = connect(mapStateToProps)(TwoPageKeypad);
