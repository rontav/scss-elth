/**
 * A component that renders an icon for a symbol with the given name.
 */

const React = require('react');

const MathIcon = require('./math-icon');
const SvgIcon = require('./svg-icon');
const TextIcon = require('./text-icon');
const { IconTypes } = require('../consts');
const { iconPropType } = require('./prop-types');

class Icon extends React.PureComponent {
    static propTypes = {
        focused: React.PropTypes.bool,
        icon: iconPropType.isRequired,
        // An Aphrodite style object, or an array of Aphrodite style objects.
        // Note that custom styles will only be applied to text and math icons
        // (and not SVG icons).
        style: React.PropTypes.any,
    }
    render() {
        const { icon, style } = this.props;

        switch (icon.type) {
            case IconTypes.MATH:
                return <MathIcon
                    math={icon.data}
                    style={style}
                />;

            case IconTypes.SVG:
                // TODO(charlie): Support passing style objects to `SvgIcon`.
                // This will require migrating the individual icons to use
                // `currentColor` and accept a `className` prop, rather than
                // relying on an explicit color prop.
                return <SvgIcon name={icon.data} />;

            case IconTypes.TEXT:
                return <TextIcon
                    character={icon.data}
                    style={style}
                />;
        }

        throw new Error('No icon or symbol provided');
    }
}

module.exports = Icon;
