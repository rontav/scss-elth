/**
 * A component that renders a text-based icon.
 */

const React = require('react');

const TextIcon = React.createClass({
    propTypes: {
        character: React.PropTypes.string.isRequired,
        style: React.PropTypes.any,
    },
    render() {
        const { character, style } = this.props;

        return (
            <div className="view row centered textIcon icon" style={style}>
                <span className="textNode">
                    {character}
                </span>
            </div>
        );
    },
});

module.exports = TextIcon;
