/**
 * A component that renders a single SVG icon.
 */

const React = require('react');

const Iconography = require('./iconography');

class SvgIcon extends React.PureComponent {
    static propTypes = {
        name: React.PropTypes.string.isRequired
    }
    render() {
        const SvgForName = Iconography[this.props.name];
        return <SvgForName color="#3B3E40" />;
    }
}

module.exports = SvgIcon;
