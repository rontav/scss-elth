import React from 'react';
import classnames from 'classnames';

const cursorRadiusPx = 11;
const cursorHeightPx = 26.5;
const cursorWidthPx = 22;

const CursorHandle = React.createClass({
    propTypes: {
        animateIntoPosition: React.PropTypes.bool,
        onTouchCancel: React.PropTypes.func.isRequired,
        onTouchEnd: React.PropTypes.func.isRequired,
        onTouchMove: React.PropTypes.func.isRequired,
        onTouchStart: React.PropTypes.func.isRequired,
        visible: React.PropTypes.bool.isRequired,
        x: React.PropTypes.number.isRequired,
        y: React.PropTypes.number.isRequired,
    },

    getDefaultProps() {
        return {
            animateIntoPosition: false,
            visible: false,
            x: 0,
            y: 0,
        };
    },

    render() {
        const { x, y } = this.props;

        const outerStyle = {
            transform: `translate(${x}px, ${y}px)`,
        };

        var classes = classnames({
            cursorIndicator: true,
            animateIntoPosition: this.props.animateIntoPosition
        });

        return <span
            className={classes}
            style={outerStyle}
            onTouchStart={this.props.onTouchStart}
            onTouchMove={this.props.onTouchMove}
            onTouchEnd={this.props.onTouchEnd}
            onTouchCancel={this.props.onTouchCancel}
        >
            <svg
                viewBox={
                    `-${cursorRadiusPx} 0 ${cursorWidthPx} ${cursorHeightPx}`
                }
            >
                <path
                    d={
                        `M 0 0
                        L -${0.707 * cursorRadiusPx} ${0.707 * cursorRadiusPx}
                        A ${cursorRadiusPx} ${cursorRadiusPx}, 0, 1, 0,
                        ${0.707 * cursorRadiusPx} ${0.707 * cursorRadiusPx}
                        Z`
                    }
                />
            </svg>
        </span>;
    },
});

export default CursorHandle;
