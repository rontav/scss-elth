/**
 * A view pager that allows for pagination in the horizontal direction.
 * Right now, there are a number of limitations built into the system. Namely:
 *  - It only supports pagination in the horizontal direction.
 *  - It supports exactly two pages.
 */

const React = require('react');
const { connect } = require('react-redux');

const { childrenPropType } = require('./prop-types');

const ViewPager = React.createClass({
    propTypes: {
        // Whether the page should animate to its next specified position.
        animateToPosition: React.PropTypes.bool,
        children: childrenPropType,
        pageWidthPx: React.PropTypes.number.isRequired,
        translateX: React.PropTypes.number.isRequired,
    },

    getInitialState() {
        return {
            animationDurationMs: 0,
        };
    },

    componentWillReceiveProps(newProps) {
        // Compute the appropriate animation length, if the pager should
        // animate to its next position.
        let animationDurationMs;
        if (newProps.animateToPosition) {
            const finalTranslateX = newProps.translateX;
            const prevTranslateX = this.props.translateX;

            // We animate at a rate of 1 pixel per millisecond, and thus we can
            // use the displacement as the animation duration.
            animationDurationMs = Math.abs(finalTranslateX - prevTranslateX);
        }
        else {
            animationDurationMs = 0;
        }
        this.setState({
            animationDurationMs,
        });
    },

    render() {
        const { children, pageWidthPx, translateX } = this.props;
        const { animationDurationMs } = this.state;

        const transform = {
            msTransform: `translate3d(${translateX}px, 0, 0)`,
            WebkitTransform: `translate3d(${translateX}px, 0, 0)`,
            transform: `translate3d(${translateX}px, 0, 0)`,
        };
        const animate = animationDurationMs ? {
            msTransitionProperty: 'transform',
            WebkitTransitionProperty: 'transform',
            transitionProperty: 'transform',
            msTransitionDuration: `${animationDurationMs}ms`,
            WebkitTransitionDuration: `${animationDurationMs}ms`,
            transitionDuration: `${animationDurationMs}ms`,
            msTransitionTimingFunction: 'ease-out',
            WebkitTransitionTimingFunction: 'ease-out',
            transitionTimingFunction: 'ease-out',
        } : {};
        const dynamicPagerStyle = {
            ...transform,
            ...animate,
        };

        const dynamicPageStyle = {
            width: pageWidthPx,
        };

        return (
            <div className="view pager" style={dynamicPagerStyle}>
                <div className="view row" style={dynamicPageStyle}>
                    {children[0]}
                </div>
                <div className="view row borderLeft" style={dynamicPageStyle}>
                    {children[1]}
                </div>
            </div>
        );
    },
});

const mapStateToProps = (state) => {
    const { animateToPosition, currentPage, dx, pageWidthPx } = state.pager;
    return {
        animateToPosition,
        pageWidthPx,
        translateX: -currentPage * (pageWidthPx + 1) + dx,
    };
};

module.exports = connect(mapStateToProps)(ViewPager);
