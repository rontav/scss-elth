/**
 * A component that renders an icon with math (via KaTeX).
 */

const React = require('react');
const ReactDOM = require('react-dom');
import KaTeX from 'katex';

const MathIcon = React.createClass({
    propTypes: {
        math: React.PropTypes.string.isRequired,
        style: React.PropTypes.any,
    },

    componentDidMount() {
        this._renderMath();
    },

    componentDidUpdate(prevProps) {
        if (prevProps.math !== this.props.math) {
            this._renderMath();
        }
    },

    _renderMath() {
        const { math } = this.props;
        KaTeX.render(math, ReactDOM.findDOMNode(this));
    },

    render() {
        return <div className="view row centered icon" style={this.props.style} />;
    },
});

module.exports = MathIcon;
