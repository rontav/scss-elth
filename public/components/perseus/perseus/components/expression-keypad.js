/**
 * A keypad that includes all of the expression symbols.
 */

const React = require('react');
const { connect } = require('react-redux');

const TwoPageKeypad = require('./two-page-keypad');
const TouchableKeypadButton = require('./touchable-keypad-button');
const { BorderStyles } = require('../consts');
const { cursorContextPropType } = require('./prop-types');
const KeyConfigs = require('../data/key-configs');
const CursorContexts = require('./input/cursor-contexts');

const ExpressionKeypad = React.createClass({
    propTypes: {
        currentPage: React.PropTypes.number.isRequired,
        cursorContext: cursorContextPropType.isRequired,
        dynamicJumpOut: React.PropTypes.bool,
        roundTopLeft: React.PropTypes.bool,
        roundTopRight: React.PropTypes.bool,
    },

    statics: {
        rows: 4,
        columns: 5,
        // Though we include an infinite-key popover in the bottom-left, it's
        // assumed that we don't need to accommodate cases in which that key
        // contains more than four children.
        maxVisibleRows: 4,
        numPages: 2,
    },

    render() {
        const {
            currentPage,
            cursorContext,
            dynamicJumpOut,
            roundTopLeft,
            roundTopRight,
        } = this.props;

        let dismissOrJumpOutKey;
        if (dynamicJumpOut) {
            switch (cursorContext) {
                case CursorContexts.IN_PARENS:
                    dismissOrJumpOutKey = KeyConfigs.JUMP_OUT_PARENTHESES;
                    break;

                case CursorContexts.IN_SUPER_SCRIPT:
                    dismissOrJumpOutKey = KeyConfigs.JUMP_OUT_EXPONENT;
                    break;

                case CursorContexts.IN_SUB_SCRIPT:
                    dismissOrJumpOutKey = KeyConfigs.JUMP_OUT_BASE;
                    break;

                case CursorContexts.BEFORE_FRACTION:
                    dismissOrJumpOutKey = KeyConfigs.JUMP_INTO_NUMERATOR;
                    break;

                case CursorContexts.IN_NUMERATOR:
                    dismissOrJumpOutKey = KeyConfigs.JUMP_OUT_NUMERATOR;
                    break;

                case CursorContexts.IN_DENOMINATOR:
                    dismissOrJumpOutKey = KeyConfigs.JUMP_OUT_DENOMINATOR;
                    break;

                case CursorContexts.NONE:
                default:
                    dismissOrJumpOutKey = KeyConfigs.DISMISS;
                    break;
            }
        }
        else {
            dismissOrJumpOutKey = KeyConfigs.DISMISS;
        }

        const rightPage = (
            <div className="view row fullWidth page roundTopRight">
                <div className="view grow">
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NUM_7}
                        borders={BorderStyles.NONE}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NUM_4}
                        borders={BorderStyles.NONE}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NUM_1}
                        borders={BorderStyles.BOTTOM}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.j}
                        borders={BorderStyles.NONE}
                    />
                </div>
                <div className="view grow">
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NUM_8}
                        borders={BorderStyles.NONE}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NUM_5}
                        borders={BorderStyles.NONE}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NUM_2}
                        borders={BorderStyles.NONE}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NUM_0}
                        borders={BorderStyles.LEFT}
                    />
                </div>
                <div className="view grow">
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NUM_9}
                        borders={BorderStyles.NONE}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NUM_6}
                        borders={BorderStyles.NONE}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NUM_3}
                        borders={BorderStyles.BOTTOM}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.DECIMAL}
                        borders={BorderStyles.LEFT}
                    />
                </div>
                <div className="view grow">
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.FRAC_INCLUSIVE}
                        borders={BorderStyles.LEFT}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.CDOT}
                        borders={BorderStyles.LEFT}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.MINUS}
                        borders={BorderStyles.LEFT}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.PLUS}
                        borders={BorderStyles.LEFT}
                    />
                </div>
                <div className="view grow">
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NOOP}
                        borders={BorderStyles.LEFT}
                        classNames={roundTopRight ? 'roundTopRight' : ''}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NOOP}
                        borders={BorderStyles.LEFT}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.BACKSPACE}
                        borders={BorderStyles.LEFT}
                    />
                    <TouchableKeypadButton
                        keyConfig={dismissOrJumpOutKey}
                        borders={BorderStyles.LEFT}
                    />
                </div>
            </div>
        );

        const leftPage = (
            <div className="view row fullWidth page roundTopLeft">
                <div className="view grow">
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.EXP_2}
                        borders={BorderStyles.BOTTOM}
                        classNames={roundTopLeft ? 'roundTopLeft' : ''}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.SQRT}
                        borders={BorderStyles.BOTTOM}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NOOP}
                        borders={BorderStyles.BOTTOM}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.SIN}
                        borders={BorderStyles.NONE}
                    />
                </div>
                <div className="view grow">
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.EXP_3}
                        borders={BorderStyles.BOTTOM}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.CUBE_ROOT}
                        borders={BorderStyles.BOTTOM}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.LN}
                        borders={BorderStyles.BOTTOM}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.COS}
                        borders={BorderStyles.NONE}
                    />
                </div>
                <div className="view grow">
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.EXP}
                        borders={BorderStyles.BOTTOM}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.RADICAL}
                        borders={BorderStyles.BOTTOM}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.LOG_N}
                        borders={BorderStyles.BOTTOM}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.TAN}
                        borders={BorderStyles.NONE}
                    />
                </div>
                <div className="view grow">
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.e}
                        borders={BorderStyles.ALL}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NOOP}
                        borders={BorderStyles.ALL}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NOOP}
                        borders={BorderStyles.ALL}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.LEFT_PAREN}
                        borders={BorderStyles.LEFT}
                    />
                </div>
                <div className="view grow">
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NOOP}
                        borders={BorderStyles.ALL}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NOOP}
                        borders={BorderStyles.ALL}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.NOOP}
                        borders={BorderStyles.ALL}
                    />
                    <TouchableKeypadButton
                        keyConfig={KeyConfigs.RIGHT_PAREN}
                        borders={BorderStyles.NONE}
                    />
                </div>
            </div>
        );

        return <TwoPageKeypad
            currentPage={currentPage}
            rightPage={rightPage}
            leftPage={leftPage}
        />;
    },
});

const mapStateToProps = (state) => {
    return {
        currentPage: state.pager.currentPage,
        cursorContext: state.input.cursor.context,
        dynamicJumpOut: !state.layout.navigationPadEnabled,
    };
};

module.exports = connect(mapStateToProps)(ExpressionKeypad);
