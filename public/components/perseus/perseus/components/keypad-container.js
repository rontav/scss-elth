const React = require('react');
const { connect } = require('react-redux');

const ExpressionKeypad = require('./expression-keypad');
const { setPageSize } = require('../actions');
const { KeypadTypes, LayoutModes } = require('../consts');
import classnames from 'classnames';

const KeypadContainer = React.createClass({
    propTypes: {
        active: React.PropTypes.bool,
        keypadType: React.PropTypes.oneOf(Object.keys(KeypadTypes)).isRequired,
        layoutMode: React.PropTypes.oneOf(Object.keys(LayoutModes)).isRequired,
        navigationPadEnabled: React.PropTypes.bool.isRequired,
        onDismiss: React.PropTypes.func,
        // A callback that should be triggered with the root React element on
        // mount.
        onElementMounted: React.PropTypes.func,
        paginationEnabled: React.PropTypes.bool.isRequired,
        onPageSizeChange: React.PropTypes.func.isRequired,
    },

    getInitialState() {
        // Use (partially unsupported) viewport units until componentDidMount.
        // It's okay to use the viewport units since they'll be overridden as
        // soon as the JavaScript kicks in.
        return {
            hasBeenActivated: false,
            viewportWidth: '100vw',
        };
    },

    componentWillMount() {
        if (this.props.active) {
            this.setState({
                hasBeenActivated: this.props.active,
            });
        }
    },

    componentDidMount() {
        // Relay the initial size metrics.
        this._onResize();

        // And update it on resize.
        window.addEventListener('resize', this._throttleResizeHandler);
        window.addEventListener(
            'orientationchange', this._throttleResizeHandler
        );
    },

    componentWillReceiveProps(nextProps) {
        if (!this.state.hasBeenActivated && nextProps.active) {
            this.setState({
                hasBeenActivated: true,
            });
        }
    },

    componentDidUpdate(prevProps) {
        if (prevProps.active && !this.props.active) {
            this.props.onDismiss && this.props.onDismiss();
        }
    },

    componentWillUnmount() {
        window.removeEventListener('resize', this._throttleResizeHandler);
        window.removeEventListener(
            'orientationchange', this._throttleResizeHandler
        );
    },

    _throttleResizeHandler() {
        // Throttle the resize callbacks.
        // https://developer.mozilla.org/en-US/docs/Web/Events/resize
        if (this._resizeTimeout == null) {
            this._resizeTimeout = setTimeout(() => {
                this._resizeTimeout = null;

                this._onResize();
            }, 66);
        }
    },

    _onResize() {
        // Whenever the page resizes, we need to force an update, as the button
        // heights and keypad width are computed based on horizontal space.
        this.setState({
            viewportWidth: window.innerWidth,
        });

        this.props.onPageSizeChange(window.innerWidth, window.innerHeight);
    },
    render() {
        const {
            active,
            onElementMounted,
            layoutMode,
            navigationPadEnabled,
            paginationEnabled
        } = this.props;
        const { hasBeenActivated } = this.state;

        const keypadProps = {
            // HACK(charlie): In order to properly round the corners of the
            // compact keypad, we need to instruct some of our child views to
            // crop themselves. At least we're colocating all the layout
            // information in this component, though.
            roundTopLeft: layoutMode === LayoutModes.COMPACT &&
            !navigationPadEnabled,
            roundTopRight: layoutMode === LayoutModes.COMPACT,
        };

        var classes = classnames({
            keypadContainer: true,
            active: active,
            invisible: !active && !hasBeenActivated,
            centered: !paginationEnabled
        });

        return (
            <div className={classes}>
                <div
                    className="keypad"
                    ref={element => {
                        if (!this.hasMounted && element) {
                            this.hasMounted = true;
                            onElementMounted(element);
                        }
                    }}
                >
                    <ExpressionKeypad {...keypadProps} />
                </div>
            </div>
        );
    },
});

const mapStateToProps = (state) => {
    return {
        ...state.keypad,
        layoutMode: state.layout.layoutMode,
        paginationEnabled: state.layout.paginationEnabled,
        navigationPadEnabled: state.layout.navigationPadEnabled,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onPageSizeChange: (pageWidthPx, pageHeightPx) => {
            dispatch(setPageSize(pageWidthPx, pageHeightPx));
        },
    };
};

module.exports = connect(mapStateToProps, mapDispatchToProps)(KeypadContainer);
