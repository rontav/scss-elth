/* eslint-disable comma-dangle, no-var, object-curly-spacing, react/jsx-closing-bracket-location, react/jsx-indent-props, react/prop-types, react/sort-comp */
/* TODO(csilvers): fix these lint errors (http://eslint.org/docs/rules): */
/* To fix, remove an entry above, run ka-lint, and fix errors. */

var React = require('react');
var _ = require('underscore');
import TeX from '../../katex.js';

var size110 = { fontSize: '110%' };
var size120 = { fontSize: '120%' };
var size130 = { fontSize: '130%' };
var size150 = { fontSize: '150%' };

// These are functions because we want to generate a new component for each use
// on the page rather than reusing an instance (which will cause an error).
// Also, it's useful for things which might look different depending on the
// props.

var basic = [
    () => [<span key="plus" style={size120}>+</span>, '+'],
    () => [<span key="minus" style={size150}>-</span>, '-'],

    // TODO(joel) - display as \cdot when appropriate
    props => {
        if (props.convertDotToTimes) {
            return [
                <TeX key="times" style={size150}>\times</TeX>,
                '\\times'
            ];
        }
        else {
            return [<TeX key="times" style={size150}>\cdot</TeX>, '\\cdot'];
        }
    },
    () => [
        <div className="katexWrapper amsrm" style={size110} key="frac"><span className="katex-display"><span className="katex"><span className="katex-html" aria-hidden="true"><span className="strut" style={{ height: 26 }}></span><span className="strut bottom"></span><span className="base displaystyle textstyle uncramped"><span className="mord reset-textstyle displaystyle textstyle uncramped"><span className="mopen sizing reset-size5 size5 reset-textstyle textstyle uncramped nulldelimiter"></span><span className="mfrac"><span className="vlist" style={{ verticalAlign: 4, marginLeft: 1 }}><span style={{ top: 8 }}><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: 0 }}>&#8203;</span></span><span className="reset-textstyle textstyle cramped"><span className="mord textstyle cramped"><span className="mord mathbb">□</span></span></span></span><span style={{ top: -6 }}><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: 0 }}>&#8203;</span></span><span className="reset-textstyle textstyle uncramped frac-line"></span></span><span style={{ top: -9 }}><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: 0 }}>&#8203;</span></span><span className="reset-textstyle textstyle uncramped"><span className="mord textstyle uncramped"><span className="mord mathbb">□</span></span></span></span><span className="baseline-fix"><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: 0 }}>&#8203;</span></span>&#8203;</span></span></span><span className="mclose sizing reset-size5 size5 reset-textstyle textstyle uncramped nulldelimiter"></span></span></span></span></span></span></div>,

        // If there's something in the input that can become part of a
        // fraction, typing "/" puts it in the numerator. If not, typing
        // "/" does nothing. In that case, enter a \frac.
        input => {
            var contents = input.latex();
            input.typedText('/');
            if (input.latex() === contents) {
                input.cmd('\\frac');
            }
        }
    ]
];

var buttonSets = {
    basic,

    'basic+div': basic.concat([
        () => [<TeX key="div">\div</TeX>, '\\div']
    ]),

    trig: [
        () => [<TeX key="sin">\sin</TeX>, '\\sin'],
        () => [<TeX key="cos">\cos</TeX>, '\\cos'],
        () => [<TeX key="tan">\tan</TeX>, '\\tan'],
        // () => [<TeX key="theta" style={symbStyle}>\theta</TeX>, '\\theta'],
        // () => [<TeX key="pi" style={symbStyle}>\phi</TeX>, '\\phi']
    ],

    prealgebra: [
        () => [<TeX key="sqrt">{'\\sqrt{x}'}</TeX>, '\\sqrt'],
        // TODO(joel) - how does desmos do this?
        () => [
            <div className="katexWrapper"><span className="katex-display"><span className="katex"><span className="katex-html" aria-hidden="true"><span className="strut" style={{ height: '0.849155em' }}></span><span className="strut bottom" style={{ height: '1.04em', verticalAlign: '-0.190845em' }}></span><span className="base displaystyle textstyle uncramped"><span className="mord sqrt"><span className="root"><span className="vlist"><span style={{ top: '-0.394986em' }}><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: 0 }}>&#8203;</span></span><span className="reset-textstyle scriptscriptstyle uncramped mtight"><span className="mord scriptscriptstyle uncramped mtight"><span className="mord mathrm mtight" style={{ marginLeft: -3, fontSize: 10 }}>3</span></span></span></span><span className="baseline-fix"><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: 0 }}>&#8203;</span></span>&#8203;</span></span></span><span className="sqrt-sign" style={{ top: '-0.009155em' }}><span className="style-wrap reset-textstyle textstyle uncramped">√</span></span><span className="vlist"><span style={{ top: 0 }}><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: '1em' }}>&#8203;</span></span><span className="mord displaystyle textstyle cramped"><span className="mord mathit">x</span></span></span><span style={{ top: '-0.769155em' }}><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: '1em' }}>&#8203;</span></span><span className="reset-textstyle textstyle uncramped sqrt-line"></span></span><span className="baseline-fix"><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: '1em' }}>&#8203;</span></span>&#8203;</span></span></span></span></span></span></span></div>,
            input => {
                input.typedText('nthroot3');
                input.keystroke('Right');
            }],
        () => [
            <div key="pow" className="katexWrapper amsrm" style={size120}><span className="katex-display"><span className="katex"><span className="katex-html" aria-hidden="true"><span className="strut" style={{ height: '0.714392em' }}></span><span className="strut bottom" style={{ height: '0.714392em', verticalAlign: 0 }}></span><span className="base displaystyle textstyle uncramped"><span className="mord"><span className="mord mathbb">□</span><span className="msupsub"><span className="vlist"><span style={{ top: '-0.413em', marginRight: -3 }}><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: 0 }}>&#8203;</span></span><span className="reset-textstyle scriptstyle uncramped mtight"><span className="mord mathit mtight" style={{ marginLeft: 2 }}>a</span></span></span><span className="baseline-fix"><span className="fontsize-ensurer reset-size5 size5"><span style={{ fontSize: 0 }}>&#8203;</span></span>&#8203;</span></span></span></span></span></span></span></span></div>,
            input => {
                var contents = input.latex();
                input.typedText('^');

                // If the input hasn't changed (for example, if we're
                // attempting to add an exponent on an empty input or an empty
                // denominator), insert our own "a^b"
                if (input.latex() === contents) {
                    input.typedText('a^b');
                }
            }
        ],
        () => [<TeX key="pi" style={size150}>\pi</TeX>, '\\pi'],
    ],

    logarithms: [
        () => [<TeX key="log">\log</TeX>, '\\log'],
        () => [<TeX key="ln">\ln</TeX>, '\\ln'],
        () => [
            <TeX key="log_b">\log_b</TeX>,
            input => {
                input.typedText('log_');
                input.keystroke('Right');
                input.typedText('(');
                input.keystroke('Left');
                input.keystroke('Left');
            }],
    ],

    'basic relations': [
        () => [<TeX key="eq">{'='}</TeX>, '='],
        () => [<TeX key="lt">\lt</TeX>, '\\lt'],
        () => [<TeX key="gt">\gt</TeX>, '\\gt'],
    ],

    'advanced relations': [
        () => [<TeX key="neq">\neq</TeX>, '\\neq'],
        () => [<TeX key="leq">\leq</TeX>, '\\leq'],
        () => [<TeX key="geq">\geq</TeX>, '\\geq'],
    ],
};

var buttonSetsType = React.PropTypes.arrayOf(
    React.PropTypes.oneOf(_(buttonSets).keys())
);

var TexButtons = React.createClass({
    propTypes: {
        sets: buttonSetsType.isRequired,
        onInsert: React.PropTypes.func.isRequired
    },

    render: function () {
        // Always show buttonSets in the same order. Note: Technically it's ok
        // for _.keys() to return the keys in an arbitrary order, but in
        // practice, they will be ordered as listed above.
        var sortedButtonSets = _.sortBy(this.props.sets,
            (setName) => _.keys(buttonSets).indexOf(setName));

        var buttons = _(sortedButtonSets).map(setName => buttonSets[setName]);

        var buttonRows = _(buttons).map(row => row.map(symbGen => {
            // create a (component, thing we should send to mathquill) pair
            var symbol = symbGen(this.props);
            return <button onClick={() => this.props.onInsert(symbol[1])}
                className="tex-button"
                key={symbol[0].key}
                tabIndex={-1}
                type="button">
                {symbol[0]}
            </button>;
        }));

        var buttonPopup = _(buttonRows).map((row, i) => {
            return <div className="clearfix tex-button-row"
                key={this.props.sets[i]}>
                {row}
            </div>;
        });

        return <div className={`${this.props.className} preview-measure`}>
            {buttonPopup}
        </div>;
    },

    statics: {
        buttonSets,
        buttonSetsType
    }
});

module.exports = TexButtons;
