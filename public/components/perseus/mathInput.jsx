import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import MathInput from './perseus/math-input.jsx';
import KeypadInput from './keyboardInput.jsx';

import KAS from 'kas';
import { debounce } from 'lodash';

import '../../scss/keyboard.scss';

class Expression extends React.Component {
    static propTypes = {
        buttonSets: PropTypes.array,
        buttonsVisible: PropTypes.oneOf(['always', 'never', 'focused']),
        keypadElement: PropTypes.any,
        times: PropTypes.bool,
        isMobile: PropTypes.bool,
        onChange: PropTypes.func.isRequired,
        onBlur: PropTypes.func,
        onFocus: PropTypes.func,
        value: PropTypes.string,
        disabled: PropTypes.bool
    }
    static defaultProps = {
        value: '',
        times: false,
        buttonSets: ['basic', 'trig', 'prealgebra', 'logarithms'],
        buttonsVisible: 'focused',
        onFocus: function () { },
        onBlur: function () { },
        onChange: function () { }
    }
    constructor() {
        super();
        this.state = {
            showErrorTooltip: false,
            showErrorText: false
        };

        this.focusMobile = this.focusMobile.bind(this);
        this.blurMobile = this.blurMobile.bind(this);
        this.handleMobileChange = this.handleMobileChange.bind(this);
    }
    parse(value) {
        return KAS.parse(value);
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.focused && !nextProps.isMobile) {
            this.blurMobile();
        }

        if (this.props.value != nextProps.value) {
            if (this.parse(nextProps.value).parsed) {
                this.setState({ showErrorTooltip: false });
            }
            else {
                this.setState({ showErrorTooltip: true });
            }
        }
    }
    focusMobile() {
        this.state.focused = true;
        window.keypad.activate(this.refs.input);
    }
    blurMobile() {
        this.state.focused = false;
        window.keypad.dismiss();
    }
    handleMobileChange(value, cb) {
        this.props.onChange(value);
        cb();
    }
    render() {
        if (this.props.isMobile) {
            return <KeypadInput
                ref="input"
                disabled={this.props.disabled}
                value={this.props.value}
                keypadElement={this.props.keypadElement}
                onChange={this.handleMobileChange}
                onFocus={this.focusMobile}
                onBlur={this.blurMobile}
            />;
        }

        var className = classnames({
            'mathInputDesktop': true,
            disabled: this.props.disabled
        });

        return <span className={className}>
            <MathInput
                ref="input"
                disabled={this.props.disabled}
                value={this.props.value}
                onChange={this.props.onChange}
                convertDotToTimes={this.props.times}
                buttonsVisible={this.props.buttonsVisible}
                buttonSets={this.props.buttonSets}
                onFocus={this.props.onFocus}
                onBlur={this.props.onBlur} />
        </span>;
    }
}

class ExpressionWrapper extends React.Component {
    static propTypes = {
        value: PropTypes.string,
        onChange: PropTypes.func,
        uid: PropTypes.any,
        disabled: PropTypes.bool
    }
    constructor(props) {
        super();
        this.state = {
            value: props.value,
            isMobile: navigator.userAgent.indexOf('Mobile') != -1
        };
        window.keypad.isMobile = this.state.isMobile;
        this.handleResize = debounce(this._handleResize.bind(this), 100);
        this.onExpressionChange = this.onExpressionChange.bind(this);
    }
    componentDidMount() {
        window.addEventListener('resize', this.handleResize);
    }
    componentWillUnmount() {
        window.keypad.dismiss(true);
        window.removeEventListener('resize', this.handleResize);
    }
    _handleResize(stopCalling) {
        var isMobile = navigator.userAgent.indexOf('Mobile') != -1;
        if (isMobile != this.state.isMobile) {
            window.keypad.setActive(0);
        }
        if (!isMobile) {
            window.keypad.dismiss();
        }
        if (window.innerWidth < 768 && !isMobile && stopCalling !== true) {
            setTimeout(() => {
                this._handleResize(true);
            }, 100);
        }
        else {
            this.setState({
                isMobile: isMobile
            });
        }
        window.keypad.isMobile = isMobile;
    }
    onExpressionChange(e) {
        this.state.value = e;
        if (this.props.onChange) {
            this.props.onChange(this.state.value);
        }

        this.forceUpdate();
    }
    render() {
        const { isMobile } = this.state;

        var classes = classnames({
            keyboardInputWrapper: true,
            mobile: isMobile
        });

        return (
            <div className={classes}>
                <Expression
                    ref="expression"
                    disabled={this.props.disabled}
                    value={this.state.value}
                    onChange={this.onExpressionChange}
                    isMobile={isMobile}
                    keypadElement={window.keypad}
                />
            </div>
        );
    }
}

export default ExpressionWrapper;
