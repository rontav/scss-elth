import React from 'react';
import { Provider } from 'react-redux';

const KeypadContainer = require('./perseus/components/keypad-container');
const {
    activateKeypad,
    dismissKeypad,
    configureKeypad,
    setCursor,
    setKeyHandler,
} = require('./perseus/actions');
const createStore = require('./perseus/store');

class ProvidedKeypad extends React.Component {
    static propTypes = {
        onElementMounted: React.PropTypes.func,
    }
    constructor() {
        super();
        this.active = 0;

        var functions = [
            'activate',
            'dismiss',
            'setActive',
            'configure',
            'setCursor',
            'setKeyHandler',
            'handleHash',
            'saveScroll'
        ];
        functions.forEach(fn => {
            this[fn] = this[fn].bind(this);
        });
    }
    componentWillMount() {
        var url = window.location.toString();
        history.replaceState({}, document.title, url.substr(0, url.indexOf('#')));

        this.store = createStore();
        this.store.dispatch(configureKeypad({
            keypadType: 'EXPRESSION'
        }));
        this.store.subscribe(() => {
            var state = this.store.getState();

            if (!state.keypad.active && location.hash && this.canGoBack) {
                history.go(-1);
                this.canGoBack = false;
            }
            if (this.active > 0 && !state.keypad.active) {
                if (window.keypad.dismissCallback) {
                    window.keypad.dismissCallback();
                }
            }
        });
        window.addEventListener('scroll', this.saveScroll, {
            passive: true
        });
        window.addEventListener('hashchange', this.handleHash);
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.saveScroll);
        window.removeEventListener('hashchange', this.handleHash);
    }
    saveScroll() {
        this.scrollX = window.scrollX;
        this.scrollY = window.scrollY;
    }
    handleHash() {
        window.scrollTo(this.scrollX, this.scrollY);
        if (location.hash.indexOf('keyboard') != -1) {
            this.active = 0;
            this.activate();
        }
        else {
            this.canGoBack = false;
            this.active = 2;
            this.store.dispatch(dismissKeypad());
        }
    }
    setActive(active) {
        this.active = active;
    }
    activate(keyboardInput) {
        if (!window.keypad.isMobile) {
            return;
        }
        if (!keyboardInput && this.keyboardInput) {
            this.keyboardInput.focus();
            return;
        }
        if (!keyboardInput && !this.keyboardInput) {
            history.go(-1);
            this.canGoBack = false;
        }

        this.active++;
        if (this.active > 0) {
            if (keyboardInput) {
                if (this.keyboardInput && keyboardInput != this.keyboardInput) {
                    this.keyboardInput.blur();
                }
                this.keyboardInput = keyboardInput;
            }
            if (this.keyboardInput) {
                var containerBounds = this.keyboardInput._mathContainer.getBoundingClientRect();
                var containerBottomPx = containerBounds.bottom;
                var containerTopPx = containerBounds.top;
                var desiredMarginPx = 28;

                var pageHeightPx = window.innerHeight;
                var keypadHeightPx = window.keypadHeight;
                var keypadTopPx = pageHeightPx - keypadHeightPx;

                if (containerBottomPx > keypadTopPx) {
                    var scrollOffset = Math.min(
                        containerBottomPx - keypadTopPx + desiredMarginPx,
                        containerTopPx
                    );

                    document.body.scrollTop += scrollOffset;
                }
            }

            this.store.dispatch(activateKeypad());
            location.hash = '#keyboard';
            this.canGoBack = true;
        }
    }
    dismiss(deleteInputRef) {
        if (deleteInputRef === true) {
            this.keyboardInput = undefined;
            this.active = 1;
        }
        this.active--;
        if (this.active <= 0) {
            this.store.dispatch(dismissKeypad());
        }
    }
    configure(configuration) {
        this.store.dispatch(configureKeypad(configuration));
    }
    setCursor(cursor) {
        this.store.dispatch(setCursor(cursor));
    }
    setKeyHandler(keyHandler) {
        this.store.dispatch(setKeyHandler(keyHandler));
    }
    render() {
        return (
            <Provider store={this.store}>
                <KeypadContainer
                    navigationPadEnabled={true}
                    onElementMounted={(element) => {
                        element.setActive = this.setActive;
                        element.activate = this.activate;
                        element.dismiss = this.dismiss;
                        element.configure = this.configure;
                        element.setCursor = this.setCursor;
                        element.setKeyHandler = this.setKeyHandler;

                        if (this.props.onElementMounted) {
                            this.props.onElementMounted(element);
                        }
                    }}
                />
            </Provider>
        );
    }
}

export default ProvidedKeypad;
