import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import moment from 'moment';
import { Input, Button, Select } from 'material-react';

import '../scss/results.scss';
import '../scss/catalog.scss';

function translate(s) {
    if (typeof s != 'string' || !s) {
        return s;
    }
    var translateRegex = /[ăâîșțÂĂÎȘȚ]/g;
    var translate = {
        'ă': 'a', 'â': 'a', 'î': 'i', 'ș': 's', 'ț': 't',
        'Ă': 'A', 'Â': 'A', 'Î': 'I', 'Ș': 'S', 'Ț': 'T'
    };
    return (s.replace(translateRegex, function (match) {
        return translate[match];
    }));
}


var DataTable = $.fn.DataTable;
$.extend(true, DataTable.ext.renderer, {
    pageButton: {
        _: function (settings, host, idx, buttons, page, pages) {
            var classes = settings.oClasses;
            var lang = settings.oLanguage.oPaginate;
            var aria = settings.oLanguage.oAria.paginate || {};
            var btnDisplay, btnClass, counter = 0;

            var attach = function (container, buttons) {
                var i, ien, node, button;
                var clickHandler = function (e) {
                    DataTable.ext.internal._fnPageChange(settings, e.data.action, true);
                };

                var btn = $('#buttonParent > div');
                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if ($.isArray(button)) {
                        var inner = $('<' + (button.DT_el || 'div') + '/>').appendTo(container);
                        attach(inner, button);
                    }
                    else {
                        btnDisplay = null;
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                container.append('<span class="ellipsis">&#x2026;</span>');
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                    classes.sPageButtonActive : '';
                                break;
                        }

                        if (btnDisplay !== null) {
                            node = btn.clone(true, true);

                            node.addClass(classes.sPageButton + ' ' + btnClass);
                            node.attr('aria-controls', settings.sTableId);
                            node.attr('aria-label', aria[button]);
                            node.attr('data-dt-idx', counter);
                            node.attr('tabindex', settings.iTabIndex);
                            node.attr('id', idx === 0 && typeof button === 'string' ?
                                settings.sTableId + '_' + button :
                                null);
                            node[0].insertAdjacentHTML('beforeend', btnDisplay);
                            var newDiv = $('<div>').appendTo(container);
                            node.appendTo(newDiv);

                            DataTable.ext.internal._fnBindAction(
                                node, { action: button }, clickHandler
                            );

                            counter++;
                        }
                    }
                }
            };

            attach($(host).empty(), buttons);
        }
    }
});

class Results extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        location: PropTypes.object,
        database: PropTypes.object
    }
    constructor() {
        super();
        this.state = {
            results: [],
            rerender: false,
        };
        this.dataTable = this.dataTable.bind(this);
        this.onChange = this.onChange.bind(this);
        this.tableLength = 30;
    }
    dataTable(tableElem) {
        var self = this;

        tableElem.attr('id', 'table');
        tableElem.appendTo($('.catalog'));

        this.table = $('#table').DataTable({
            bPaginate: true,
            bLengthChange: true,
            bFilter: true,
            bSort: true,
            bInfo: true,
            bAutoWidth: true,
            bResponsive: true,
            language: {
                sProcessing: 'Procesează...',
                sLengthMenu: 'Afișează _MENU_ înregistrări pe pagină',
                sZeroRecords: 'Nu am găsit nimic - ne pare rău',
                sInfo: 'Afișate de la _START_ la _END_ din _TOTAL_ înregistrări',
                sInfoEmpty: 'Afișate de la 0 la 0 din 0 Înregistrări',
                sInfoFiltered: '(filtrate dintr-un total de _MAX_ înregistrări)',
                sInfoPostFix: '',
                sSearch: 'Caută:',
                sUrl: '',
                oPaginate: {
                    sFirst: 'Prima',
                    sPrevious: 'Precedenta',
                    sNext: 'Următoarea',
                    sLast: 'Ultima'
                }
            },
            aoColumns: [
                null,
                null,
                { 'sType': 'date-euro' },
                null,
                null,
            ],
            lengthMenu: [10, 20, 30, 50, 100],
            columnDefs: [
                { 'className': 'dt-center', 'targets': [2, 3] }
            ],
            order: [2, 'desc'],
            search: {
                'caseInsensitive': false
            },
            searchDelay: 250,
            initComplete: function () {
                // move search inputs up
                var r = $('#table tfoot tr');
                $('#table thead').prepend(r);
                $('#search_0').css('text-align', 'center');

                // move "Show x per page" on bottom
                $('#table_length').insertBefore($('#table_info'));
                $('#table_info').remove();

                var tableLength = $('#table_length');
                var oldSelect = tableLength.find('select');

                var selectEvents = $._data(oldSelect[0], 'events');

                var options = [];
                [10, 20, 30, 50, 100].forEach(i => {
                    if (self.tableLength == i) {
                        options.push({
                            text: i,
                            selected: true
                        });
                    }
                    else {
                        options.push(i);
                    }
                });

                ReactDOM.render(<Select inline={true} width={100} height={30} options={options} onChange={self.onChange} />, $('#temp')[0]);

                var newSelect = $('#temp > div').detach();
                self.oldOnChange = selectEvents.change[0].handler;
                newSelect.find('select').on('change', selectEvents.change[0].handler);
                newSelect.insertBefore(oldSelect);
                oldSelect.remove();
            }
        });

        // Apply the search
        this.table.columns().eq(0).each((colIdx) => {
            $('input', this.table.column(colIdx).footer()).on('keyup change', function () {
                var value = translate(this.value);

                this.table
                    .column(colIdx)
                    .search(value)
                    .draw();
            });
        });
    }
    onChange(value) {
        this.tableLength = value;
        $('.materialSelect').attr('value', value);
        if (this.oldOnChange) {
            this.oldOnChange.call({
                type: 'option',
                nodeName: '.materialSelect',
                getAttribute: () => value
            });
        }
    }
    componentDidMount() {
        this.timelineRef = this.props.database.ref('/timeline/').orderByChild('active').equalTo(false);
        var quizzRef = this.props.database.ref('/quizzes/');

        this.timelineRef.on('value', (timeline) => {
            quizzRef.once('value', (quizzes) => {
                this.state.loaded = true;
                this.state.results = [];
                quizzes = quizzes.val();
                timeline = timeline.val() ? timeline.val() : 0;

                this.props.user.quizzes.forEach((userQuizz) => {
                    if (!timeline || !timeline[userQuizz]) {
                        return;
                    }
                    if (timeline[userQuizz].results) {
                        var result = timeline[userQuizz].results[this.props.user.uid].total.toString();
                        result += '/' + timeline[userQuizz].results[this.props.user.uid].max.toString();
                    }
                    var quizzStart = timeline[userQuizz].start;
                    var start = timeline[userQuizz].answers[this.props.user.uid].start;
                    var stop = timeline[userQuizz].answers[this.props.user.uid].stop;
                    // var start = 1;
                    // var stop = 2;
                    var quizzId = timeline[userQuizz].quiz;


                    this.state.results.push({
                        quizzTitle: quizzes[quizzId].title,
                        quizzType: quizzes[quizzId].type,
                        result: result ? result : '0?',
                        start: quizzStart,
                        duration: result ? stop - start : 0,
                    });
                    this.state.rerender = true;
                });
                this.forceUpdate(() => {
                    var clone = $('#tableReact').clone();
                    if ($('#table').length) {
                        $('#table').DataTable().destroy();
                        $('#table').remove();
                    }

                    this.dataTable(clone);
                });
            });
        });
    }
    componentWillUnmount() {
        if (this.timelineRef) {
            this.timelineRef.off();
        }
    }
    render() {
        if (!this.props.user) {
            return (
                <Redirect to={{
                    pathname: '/',
                    state: { from: this.props.location }
                }} />
            );
        }
        if (!this.state.loaded) {
            return (
                <div className="Loading">Loading</div>
            );
        }
        return (
            <div className="catalog elev">
                <div id="temp"></div>
                <div id="buttonTemp" className="hidden">
                    <div id="buttonParent">
                        <Button flat></Button>
                    </div>
                </div>
                <table id="tableReact">
                    <thead>
                        <tr className="headers">
                            <th className="quizName">Nume quiz</th>
                            <th className="quizType">Tip quiz</th>
                            <th className="date">Dată</th>
                            <th className="duration">Durată</th>
                            <th className="grade">Notă</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr className="inputs">
                            <th className="quizName"><Input type="text" placeholder="Nume quiz" /></th>
                            <th className="quizType"><Input type="text" placeholder="Tip quiz" /></th>
                            <th className="date"><Input type="text" placeholder="Dată" /></th>
                            <th className="duration"><Input type="text" placeholder="Durată" /></th>
                            <th className="grade"><Input type="text" placeholder="Notă" /></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        {
                            this.state.results.map((quiz, quizIndex) => {
                                return (
                                    <tr className="data" key={quizIndex}>
                                        <td>{translate(quiz.quizzTitle)}</td>
                                        <td>{translate(quiz.quizzType)}</td>
                                        <td>{moment.utc(quiz.start).format('DD-MM-YYYY HH:mm')}</td>
                                        <td>{moment(quiz.duration).format('mm:ss')}</td>
                                        <td>{quiz.result}</td>
                                    </tr>
                                );
                            })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Results;