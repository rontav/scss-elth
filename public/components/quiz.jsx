import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Select, Input, Button } from 'material-react';

import moment from 'moment';
import classnames from 'classnames';
import store from 'store';

import MathInput from './perseus/mathInput.jsx';
import KAS from 'kas';
import renderTextLatex from './renderTextLatex.js';

import Checkbox from 'material-ui/Checkbox';
import CheckBoxIcon from 'material-ui/svg-icons/toggle/check-box';
import CheckIcon from 'material-ui/svg-icons/navigation/check';
import TimesIcon from 'material-ui/svg-icons/content/clear';
import _ from 'lodash';

import '../scss/quiz.scss';

class FirebaseImage extends React.Component {
    static propTypes = {
        src: PropTypes.string,
    }
    constructor() {
        super();

        this.state = {
            maxWidth: '100%'
        };

        this.onLoad = this.onLoad.bind(this);
    }
    onLoad(e) {
        e.persist();
        this.setState({
            maxWidth: e.target.naturalWidth
        });
    }
    render() {
        var style = {
            maxWidth: this.state.maxWidth
        };

        return (
            <div className="firebaseImage">
                <a href={this.props.src} target="_blank" className="background" ><img style={style} src={this.props.src} onLoad={this.onLoad} /></a>
            </div>
        );
    }
}

class Question extends React.Component {
    static propTypes = {
        answers: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.object,
            PropTypes.string
        ]),
        id: PropTypes.number,
        title: PropTypes.string,
        images: PropTypes.array,
        type: PropTypes.string,
        onChange: PropTypes.func,
        uid: PropTypes.any,
        quizId: PropTypes.any,
        verified: PropTypes.any
    }
    static defaultProps = {
        onChange: () => { }
    }
    constructor(props) {
        super(props);

        this.state = {
            answer: store.get(props.quizId + ',' + props.uid)
        };

        this.getAnswer = this.getAnswer.bind(this);
        this.isChecked = this.isChecked.bind(this);
        this.onChange = this.onChange.bind(this);
        this.doAnswer = this.doAnswer.bind(this);
        this.renderAnswer = this.renderAnswer.bind(this);
    }
    getAnswer() {
        return this.state.answer;
    }
    componentWillReceiveProps(nextProps) {
        // this.setState({ answer: false });
    }
    onChange() {
        var { quizId, onChange, uid } = this.props;
        var { answer } = this.state;
        store.set(quizId + ',' + uid, answer);

        _.isFunction(onChange) && onChange(answer);
    }
    isChecked(answer) {
        var { answer: current } = this.state;
        if (!current) return false;
        if (_.isString(current)) return true;

        if (_.isArray(current)) {
            return current.indexOf(answer.id) != -1;
        }

        return current == answer.id;
    }
    doAnswer(answer, notUsed, selectObject) {
        var { type } = this.props;
        var { answer: current } = this.state;

        if (this.props.verified) {
            return;
        }

        if (type == 'text') {
            return this.setState({ answer }, this.onChange);
        }
        else if (type == 'grilă') {
            var answers = _.isArray(current) ? current : [];

            var find = answers.indexOf(answer);
            if (find != -1) answers.splice(find, 1);
            else answers.push(answer);

            return this.setState({ answer: answers }, this.onChange);
        }
        else if (type == 'select') {
            var index = answer;

            this.state.answer = this.state.answer || [];
            this.state.answer[index] = selectObject.originalText;
            return this.forceUpdate(this.onChange);
        }

        // Fallback
        return this.setState({ answer }, this.onChange);
    }
    deleteLocalStorage() {
        store.remove(this.props.quizId + ',' + this.props.uid);
    }
    renderAnswer(answer, key) {
        var { type } = this.props;
        if (_.isString(answer)) answer = { title: 'Răspuns' };
        var correct, checked, checkboxColor, checkedIcon;

        if (type == 'grilă') {
            checked = this.isChecked(answer);
            checkboxColor = '#3F51B5';
            checkedIcon = <CheckBoxIcon />;

            if (answer.correct) {
                if (answer.correct == 'correct') {
                    checkboxColor = '#4CAF50';
                    checked = true;
                    checkedIcon = <CheckIcon />;
                }
                if (answer.correct == 'shouldCheck') {
                    checkboxColor = '#FFC107';
                    checked = true;
                    checkedIcon = <CheckIcon />;
                }
                if (answer.correct == 'incorrect') {
                    checkboxColor = '#F44336';
                    checkedIcon = <TimesIcon />;
                }
            }

            return (
                <label key={key} className="answer checkbox">
                    <Checkbox
                        disableTouchRipple={true}
                        className="checkbox"
                        iconStyle={{ fill: checkboxColor }}
                        checkedIcon={checkedIcon}
                        checked={checked}
                        onCheck={() => this.doAnswer(answer.id)}
                    />
                    <div className="text">{renderTextLatex(answer.title)}</div>
                </label>
            );
        }
        else if (type == 'text') {
            correct = this.props.answers[0].correct;
            if (correct) {
                checkboxColor = '#4CAF50';
                checked = true;
                checkedIcon = <CheckIcon />;
            }
            if (!correct) {
                checkboxColor = '#F44336';
                checkedIcon = <TimesIcon />;
            }

            return (
                <div className="math-input" key={key}>
                    <div className="math-line">
                        <MathInput onChange={this.doAnswer} value={this.state.answer} disabled={correct !== undefined} />
                        {
                            correct !== undefined ?
                                <Checkbox
                                    disableTouchRipple={true}
                                    className={`checkbox${correct ? '' : ' incorrect'}`}
                                    iconStyle={{ fill: checkboxColor }}
                                    checkedIcon={checkedIcon}
                                    checked={true}
                                />
                                : ''
                        }
                    </div>
                    {
                        correct === false ?
                            <div className="checkedResponse">
                                Răspunsul corect era: {renderTextLatex('$ ' + this.props.answers[0].title + ' $')}
                            </div>
                            : ''
                    }
                </div>
            );
        }
        else if (type == 'select') {
            var options = [
                {
                    text: 'Alegeți un răspuns',
                    placeholder: true
                }
            ];

            if (!answer.answers) {
                return '';
            }
            answer.answers.forEach(answer => {
                if (this.state.answer && this.state.answer[key] == answer.title) {
                    options.push({
                        text: renderTextLatex(answer.title),
                        originalText: answer.title,
                        selected: true
                    });
                }
                else {
                    options.push({
                        text: renderTextLatex(answer.title),
                        originalText: answer.title
                    });
                }
            });

            var correctResponse;
            correct = answer.correct;
            if (correct) {
                checkboxColor = '#4CAF50';
                checked = true;
                checkedIcon = <CheckIcon />;
            }
            if (!correct) {
                checkboxColor = '#F44336';
                checkedIcon = <TimesIcon />;

                answer.answers.forEach(answer => {
                    if (answer.checked) {
                        correctResponse = answer.title;
                    }
                });
            }

            return (
                <div className="answer select" key={key}>
                    <div className="selectTitle">{renderTextLatex(answer.title)}</div>
                    <div className="selectAnswer">
                        <Select required={true} options={options} onChange={this.doAnswer.bind(this, key)} />
                    </div>
                    {
                        correct !== undefined ?
                            <Checkbox
                                disableTouchRipple={true}
                                className={`checkbox${correct ? '' : ' incorrect'}`}
                                iconStyle={{ fill: checkboxColor }}
                                checkedIcon={checkedIcon}
                                checked={true}
                            />
                            : ''
                    }
                    {
                        correct === false ?
                            <div className="checkedResponse">
                                Răspunsul corect era: <span className="textAccent">{renderTextLatex(correctResponse)}</span>
                            </div>
                            : ''
                    }
                </div>
            );
        }
    }
    render() {
        var { title, answers, type } = this.props;
        answers = !_.isArray(answers) ? [answers] : answers;

        return (
            <div className="questionWrapper">
                <div className="question">
                    <pre className="title">{renderTextLatex(title)}</pre>
                    {this.props.images ? this.props.images.map((image, index) => {
                        return (<FirebaseImage key={'image-' + index} src={image} alt="Question image" />);
                    }) : ''
                    }
                    <div className="answers">
                        {
                            type == 'text' ? this.renderAnswer() : answers.map(this.renderAnswer)
                        }
                    </div>
                </div>
            </div>
        );
    }
}

class Quiz extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        database: PropTypes.object,
        params: PropTypes.object,
        location: PropTypes.object,
        storage: PropTypes.object,
        training: PropTypes.bool
    }
    constructor() {
        super();
        this.state = {
            submitted: false,
        };
        this.getAnswers = this.getAnswers.bind(this);
        this.submitQuiz = this.submitQuiz.bind(this);
        this.waitResults = this.waitResults.bind(this);
        this.renderSubmit = this.renderSubmit.bind(this);
        this.verify = this.verify.bind(this);
    }
    verify() {
        var { quiz } = this.state;
        var answers = this.getAnswers();

        quiz.questions.forEach((question, questionIndex) => {
            question.verified = true;
            var userAnswers = answers[questionIndex];
            if (!userAnswers) {
                return;
            }

            if (question.type == 'grilă') {
                var ids = userAnswers.answer || [];
                question.answers.forEach((answer) => {
                    if (answer.checked) {
                        if (ids.indexOf(answer.id) != -1) {
                            answer.correct = 'correct';
                        }
                        else {
                            answer.correct = 'shouldCheck';
                        }
                    }
                    else if (ids.indexOf(answer.id) != -1) {
                        answer.correct = 'incorrect';
                    }
                });
            }
            if (question.type == 'text') {
                if (answers[questionIndex].answer) {
                    var currentAnswer = KAS.parse(answers[questionIndex].answer);
                    var correctForm = KAS.parse(question.answers[0].title);

                    if (!currentAnswer.parsed || !correctForm.parsed) {
                        question.answers[0].correct = false;
                    }
                    else {
                        var temp = KAS.compare(currentAnswer.expr, correctForm.expr);

                        question.answers[0].correct = temp.equal;
                    }
                }
                else {
                    question.answers[0].correct = false;
                }
            }
            if (question.type == 'select') {
                question.answers.forEach((select, selectIndex) => {
                    var selectedAnswer = userAnswers.answer[selectIndex];
                    select.answers.forEach(aa => {
                        if (select.correct === false) {
                            return;
                        }

                        if (aa.checked == (aa.title == selectedAnswer)) {
                            select.correct = true;
                        }
                        else {
                            select.correct = false;
                        }
                    });
                });
            }
        });
        this.state.submitted = true;
        window.scrollToTop();
        this.forceUpdate();
    }
    getAnswers() {
        var answers = [];

        _(this.refs).each((val, key) => {
            if (!val || key.indexOf('question-') != 0) return;
            key = key.replace('question-', '');

            answers.push({
                id: val.props.id,
                answer: val.getAnswer() ? val.getAnswer() : false
            });
        });

        return answers;
    }
    submitQuiz() {
        if (this.forceWait) {
            return;
        }
        this.forceWait = true;
        this.state.forceWait = true;
        if (this.props.training) {
            this.verify();
            this.state.quiz.questions.forEach((question, index) => {
                this.refs['question-' + index].deleteLocalStorage();
            });
            this.quizRef.off();
            return;
        }
        var answers = this.getAnswers();
        var { database: db, params, user } = this.props;

        var workSpace = '/timeline/' + params.id;

        var ref = db.ref(workSpace);

        ref.child('/answers/' + user.uid).set({
            answers: answers,
            start: this.props.training ? 0 : this.startedAt,
            stop: this.props.training ? 0 : new Date().getTime()
        }, (err) => {
            this.forceWait = true;
            this.quizRef.off();
            this.forceUpdate();
            if (err) return console.log('Cannot submit:', err);

            this._waitResults = ref;
            ref.on('value', this.waitResults);
        });
        this.forceWait = true;
        this.forceUpdate();

        this.state.quiz.questions.forEach((question, index) => {
            this.refs['question-' + index].deleteLocalStorage();
        });
        this.setState({
            submitted: true
        });
    }
    async parseQuiz(quiz) {
        quiz.questions = quiz.questions || [];

        var imagePromises = [];
        var imageMeta = [];
        this.maxResult = 0;
        quiz.questions.forEach((question, questionIndex) => {
            this.maxResult += parseInt(question.points);

            if (question.images) {
                question.images.forEach(image => {
                    imagePromises.push(
                        this.props.storage.child(image).getDownloadURL()
                    );
                    imageMeta.push({
                        questionIndex
                    });
                });
            }

            question.images = [];
            question.answers = question.answers || [];
            question.answers.forEach((answer, index) => {
                answer.id = index;
            });

            question.id = questionIndex;
        });
        var urls = await Promise.all(imagePromises);
        urls.forEach((url, index) => {
            quiz.questions[imageMeta[index].questionIndex].images.push(url);
        });
        this.state.quiz = quiz;
    }
    async componentDidMount() {
        if (this.props.training) {
            this.quizRef = this.props.database.ref('quizzes/').child(this.props.params.id);

            this.quizRef.on('value', async (quiz) => {
                await this.parseQuiz(quiz.val());
                window.keypad.dismiss(true);
                this.forceUpdate();
            });
        }
        else {
            this.timelineRef = this.props.database.ref('timeline/').child(this.props.params.id);

            this.timelineRef.on('value', async (timeline) => {
                if (this.forceWait) {
                    return;
                }
                timeline = timeline.val();
                if (timeline.answers && timeline.answers[this.props.user.uid]) {
                    this.forceWait = true;
                    this.redirect = true;
                    this.forceUpdate();
                    this.setState({
                        submitted: true
                    });
                }
                if (this.timerInterval) {
                    clearInterval(this.timerInterval);
                }

                if (!timeline) {
                    this.setState({
                        notFound: true
                    });
                    return;
                }
                timeline.stop -= 30 * 1000;
                this.timeline = timeline;

                if (!this.quizRef) {
                    this.quizRef = this.props.database.ref('quizzes/').child(timeline.quiz);

                    this.quizRef.on('value', async (quiz) => {
                        await this.parseQuiz(quiz.val());
                        this.forceUpdate();
                    });
                }

                if (!this.startedAt) {
                    this.startedAt = moment().valueOf();
                }

                this.timer = moment(timeline.stop - Date.now());

                this.timerInterval = setInterval(() => {
                    this.timer.subtract(1000);
                    this.forceUpdate();
                }, 1000);

                window.keypad.dismiss(true);
                this.forceUpdate();
            });
        }
    }
    componentWillUnmount() {
        if (this.timelineRef) {
            this.timelineRef.off();
        }
        if (this.timerInterval) {
            clearInterval(this.timerInterval);
        }
    }
    waitResults(snap) {
        var value = snap.val();

        if (_.isObject(value) && value.results) this.setState({ results: value.results });
    }
    renderSubmit() {
        var { results } = this.state;

        return (
            <div className="quizPageWrapper">
                <div className="quizPage">
                    <div className="questions"><div className="questionWrapper">
                        <div className="question">
                            {!results ? <h2 className="title">Se așteaptă rezultatele...</h2> : <div>
                                <h2 className="title">Rezultate</h2>
                                <p>Total: <strong>{results[this.props.user.uid].total} / {this.maxResult}</strong></p>
                                {/*<p>Întrebări răspunse corect: <strong>{results.answered}/{results.questions}</strong></p>*/}
                            </div>
                            }
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
    render() {
        if (this.state.notFound || this.redirect) {
            return (
                <Redirect to={{
                    pathname: '/',
                    state: { from: this.props.location }
                }} />
            );
        }
        if (!this.state.quiz) {
            return (<div className="loading">Loading</div>);
        }
        if (!this.props.training) {
            if ((this.timer && this.timer.valueOf() <= 0) || this.state.submitted || this.forceWait) {
                this.submitQuiz();
                if (this.timerInterval) {
                    clearInterval(this.timerInterval);
                }
                // TODO: Change this to a redirect or something
                return this.renderSubmit();
            }
        }

        var content = null;
        var { submitted } = this.state;

        content = this.state.quiz.questions.map((question, index) => {
            return (<Question quizId={this.props.params.id} uid={`question-${index}`} ref={'question-' + index} key={index} {...question} />);
        });

        var timerClasses = classnames({
            timer: true,
            red: this.timer && this.timer.valueOf() / 1000 < 60
        });

        return (
            <div className="quizPageWrapper">
                <div className={timerClasses}>{this.timer && this.timer.format('mm:ss')}</div>
                <div className="quizPage">
                    <div className="quizTitle">Quiz</div>
                    <div className="quizTitle"><span className="accent">{this.state.quiz.title}</span></div>
                    <div className="quizType"><span className="accent">{this.state.quiz.type}</span></div>
                    <div className="questions">
                        {content}
                    </div>
                    {
                        submitted ?
                            <div className="btnLine"><Link to="/"><Button raised>Acasă</Button></Link></div>
                            :
                            <div className="btnLine"><Button raised onClick={this.submitQuiz}>Verificare</Button></div>
                    }
                </div>
            </div>
        );
    }

}
export default Quiz;
