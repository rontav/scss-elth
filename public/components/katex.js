import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import KaTeX from 'katex';
/*
import parseTree from 'katex/src/parseTree';
import Settings from 'katex/src/Settings';
import Style from 'katex/src/Style';
import Options from 'katex/src/Options';
import buildHTML from 'katex/src/buildHTML';
import buildMathML from 'katex/src/buildMathML';
import buildCommon from 'katex/src/buildCommon';

function buildTree(tree, expression, settings, font) {
    settings = settings || new Settings({});

    var startStyle = Style.TEXT;
    if (settings.displayMode) {
        startStyle = Style.DISPLAY;
    }

    // Setup the default options
    var options = new Options({
        style: startStyle,
        size: 'size5',
        font: font
    });

    // `buildHTML` sometimes messes with the parse tree (like turning bins ->
    // ords), so we build the MathML version first.
    var mathMLNode = buildMathML(tree, expression, options);
    var htmlNode = buildHTML(tree, options);

    var katexNode = buildCommon.makeSpan(['katex'], [
        mathMLNode, htmlNode
    ]);

    if (settings.displayMode) {
        return buildCommon.makeSpan(['katex-display'], [katexNode]);
    }
    else {
        return katexNode;
    }
}

function ktx(expression, options) {
    var settings = new Settings(options);

    var tree = parseTree(expression, settings);
    return buildTree(tree, expression, settings, options.font).toMarkup();
}
window.KaTeX = KaTeX;
*/

class MathComponent extends React.Component {
    static propTypes = {
        children: PropTypes.string,
        math: PropTypes.string,
        className: PropTypes.string,
        style: PropTypes.object,
        block: PropTypes.bool,
        inline: PropTypes.bool
    }
    constructor(props) {
        super(props);

        this.state = {
            html: this.generateHtml(props)
        };
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            html: this.generateHtml(nextProps)
        });
    }
    generateHtml(props) {
        var html;
        try {
            // use this to force render mathbb chars (I've used it for \\square)
            // var font = 'mathbb';
            // html = ktx(
            //     props.children,
            //     {
            //         displayMode: true,
            //         font: font
            //     }
            // );
            html = KaTeX.renderToString(
                props.children,
                { displayMode: true }
            );
        }
        catch (e) {
            console.log(e);
            html = 'expresie invalidă';
        }
        return html;
    }
    render() {
        var classes = classnames({
            katexWrapper: true,
            block: this.props.block,
            inline: this.props.inline
        }, this.props.className);

        return <div style={this.props.style} className={classes} dangerouslySetInnerHTML={{
            __html: this.state.html
        }} />;
    }
}

export default MathComponent;
