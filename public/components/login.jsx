import React from 'react';
import PropTypes from 'prop-types';
import { Button, Input } from 'material-react';

import '../scss/login.scss';

class LoginPage extends React.Component {
    static propTypes = {
        login: PropTypes.func
    }
    constructor() {
        super();

        this.emailLogin = this.emailLogin.bind(this);
    }
    emailLogin(e) {
        if (e && e.preventDefault) {
            e.preventDefault();
        }
        var email = this.refs.email.state.value.replace(/\s+/g, '');
        if (email.indexOf('@') == -1) {
            email += '@stud.acs.upb.ro';
        }
        var password = this.refs.password.state.value;

        if (email && password) {
            this.props.login({ email: email, password: password });
        }
    }
    render() {
        return (
            <div className="loginPage">
                <h1>Bine ați venit pe <span className="accent">QuizMaster - ELTH</span></h1>
                <div className="logo" />

                <form className="mail-login" onSubmit={this.emailLogin} action="">
                    <Input required={true} placeholder='Username' ref='email' />
                    <Input type={'password'} required={true} placeholder='Password' ref='password' />
                    <div className="subtitle">
                        Folositi utilizatorul si parola de pe cs.curs
                    </div>
                    <input type="submit" value="Submit" />
                    <Button raised onClick={this.emailLogin}><span className="accent">Login</span></Button>
                </form>
            </div>
        );
    }
}

export default LoginPage;