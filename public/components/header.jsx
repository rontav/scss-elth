import React from 'react';
import { Button } from 'material-react';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import '../scss/header.scss';

class Header extends React.Component {
    static propTypes = {
        title: PropTypes.string,
        user: PropTypes.object,
        logout: PropTypes.func,
        location: PropTypes.object
    }
    constructor(props) {
        super(props);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
        this.handleResize = this.handleResize.bind(this);
        this.state = {
            menu: false,
            collapse: true
        };
    }
    toggleMenu(e) {
        this.setState({
            menu: !this.state.menu
        });
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(() => {
            this.timeout = undefined;
            this.setState({
                backdropEnable: true
            });
        }, 500);
    }
    closeMenu() {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }

        this.setState({
            menu: false,
            backdropEnable: false
        });
    }
    handleResize() {
        if (window.innerWidth > 992) {
            this.setState({
                collapse: false
            });
        }
        else {
            this.setState({
                collapse: true
            });
        }
    }
    componentDidMount() {
        window.addEventListener('resize', this.handleResize);
        this.handleResize();
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
    }
    render() {
        var classes = classnames({
            menuWrapper: true,
            big: !this.state.collapse,
            open: this.state.collapse && this.state.menu
        });

        return (
            <div className={classes}>
                <nav className="vertical">
                    <Link to="/" className="logo" onClick={this.closeMenu}><div className="image" /></Link>
                    <div className="links">
                        <Link to="/" onClick={this.closeMenu}>Acasă</Link>
                        {
                            this.props.user.uid != 'guest' ?
                                <Link to="/catalog" onClick={this.closeMenu}>Vezi notele</Link>
                                : ''
                        }
                        {
                            this.props.user.admin ?
                                <div className="forAdmin">
                                    <Link to="/admin" onClick={this.closeMenu}>Admin</Link>
                                    <Link to="/admin/chooseQuiz/" onClick={this.closeMenu}>Programeaza un test</Link>
                                    <Link to="/admin/catalog" onClick={this.closeMenu}>Catalog</Link>
                                </div>
                                : ''
                        }
                        {
                            this.props.user.uid != 'guest' ?
                                <a className="logout" href="#" onClick={this.props.logout}>Logout</a>
                                : ''
                        }
                    </div>
                </nav>
                <nav className="horizontal">
                    <Button flat className="iconBtn menu" onClick={this.toggleMenu} />
                    <div className="pageTitle">
                        {this.props.title}
                    </div>
                </nav>
                <div className="backdrop" onClick={this.state.backdropEnable ? this.closeMenu : undefined}></div>
            </div>
        );
    }
}

export default Header;