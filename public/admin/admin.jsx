import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Link } from 'react-router-dom';
import { Button } from 'material-react';

import './../scss/admin.scss';

class Admin extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        location: PropTypes.object,
        database: PropTypes.object
    }
    constructor(props) {
        super(props);
        this.quizzes = {};
        this.toEdit = {};

        this.state = {
            create: false,
            edit: this.toEdit,
            showQuizzes: {}
        };
        this.deleteQuiz = this.deleteQuiz.bind(this);
        this.editQuiz = this.editQuiz.bind(this);
    }
    deleteQuiz(id) {
        var quiz = this.props.database.ref('quizzes/' + id);
        quiz.update({
            hidden: true,
        });
    }
    editQuiz(quiz) {
        this.setState({
            edit: {
                title: quiz.title,
                type: quiz.type,
                questions: quiz.questions
            }
        });
    }
    componentDidMount() {
        this.quizzesRef = this.props.database.ref('quizzes');

        this.quizzesRef.on('value', (qu) => {
            this.setState({
                showQuizzes: qu.val()
            });
        });
    }
    render() {
        if (!this.state.showQuizzes || !Object.keys(this.state.showQuizzes).length) {
            return <div className="Loading">Loading</div>;
        }

        return (
            <div className="admin">
                <div className="quiz-list">
                    {this.state.showQuizzes ? Object.keys(this.state.showQuizzes).map((quiz, index) => {
                        if (this.state.showQuizzes[quiz].hidden) {
                            return '';
                        }
                        return <div className='quiz-card' key={index}>
                            <div className="quiz-elem title">{this.state.showQuizzes[quiz].title}</div>
                            <div className="quiz-elem type">{this.state.showQuizzes[quiz].type} </div>
                            <Link to={`/admin/edit/${quiz}`}><Button flat className="edit"><i className="fa fa-pencil-square-o"></i></Button></Link>
                            <Button flat className="delete" onClick={this.deleteQuiz.bind(this, quiz)}><i className="fa fa-trash" /></Button>
                        </div>;
                    }) : ''}
                </div>

                <div className="new-quiz">
                    <Link to={`/admin/edit/`}><Button raised><i className="fa fa-plus" aria-hidden="true"></i></Button></Link>
                </div>
            </div>
        );
    }
}
export default Admin;