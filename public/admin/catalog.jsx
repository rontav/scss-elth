import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import moment from 'moment';
import { Input, Button, Select } from 'material-react';

import '../scss/catalog.scss';

function translate(s) {
    if (typeof s != 'string' || !s) {
        return s;
    }
    var translateRegex = /[ăâîșțÂĂÎȘȚ]/g;
    var translate = {
        'ă': 'a', 'â': 'a', 'î': 'i', 'ș': 's', 'ț': 't',
        'Ă': 'A', 'Â': 'A', 'Î': 'I', 'Ș': 'S', 'Ț': 'T'
    };
    return (s.replace(translateRegex, function (match) {
        return translate[match];
    }));
}

// $(document.body).on('mousedown', '.materialButton', function (event) {
//     if ($(this).attr('class').indexOf('disabled') == -1 && parseFloat($(this).css('opacity')) > 0) {
//         var ripple;
//         var maxWidthHeight = Math.max($(this).width(), $(this).height());
//         if ($(this).find('b.ripple').length == 0 || $(this).find('b.ripple').css('opacity') != 1) {
//             ripple = $('<b class="ripple" style="width:' + maxWidthHeight + 'px;height:' + maxWidthHeight + 'px;"></b>').prependTo(this);
//         }
//         else {
//             $(this).find('b.ripple').each(function () {
//                 if ($(this).css('opacity') == 1) {
//                     ripple = $(this).removeClass('animate');
//                     return;
//                 }
//             });
//         }
//         var x = event.pageX - ripple.width() / 2 - $(this).offset().left;
//         var y = event.pageY - ripple.height() / 2 - $(this).offset().top;
//         ripple.css({
//             top: y,
//             left: x
//         }).addClass('animate');
//     }
// });

var DataTable = $.fn.DataTable;
$.extend(true, DataTable.ext.renderer, {
    pageButton: {
        _: function (settings, host, idx, buttons, page, pages) {
            var classes = settings.oClasses;
            var lang = settings.oLanguage.oPaginate;
            var aria = settings.oLanguage.oAria.paginate || {};
            var btnDisplay, btnClass, counter = 0;

            var attach = function (container, buttons) {
                var i, ien, node, button;
                var clickHandler = function (e) {
                    DataTable.ext.internal._fnPageChange(settings, e.data.action, true);
                };

                var btn = $('#buttonParent > div');
                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if ($.isArray(button)) {
                        var inner = $('<' + (button.DT_el || 'div') + '/>').appendTo(container);
                        attach(inner, button);
                    }
                    else {
                        btnDisplay = null;
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                container.append('<span class="ellipsis">&#x2026;</span>');
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                    classes.sPageButtonActive : '';
                                break;
                        }

                        if (btnDisplay !== null) {
                            node = btn.clone(true, true);

                            node.addClass(classes.sPageButton + ' ' + btnClass);
                            node.attr('aria-controls', settings.sTableId);
                            node.attr('aria-label', aria[button]);
                            node.attr('data-dt-idx', counter);
                            node.attr('tabindex', settings.iTabIndex);
                            node.attr('id', idx === 0 && typeof button === 'string' ?
                                settings.sTableId + '_' + button :
                                null);
                            node[0].insertAdjacentHTML('beforeend', btnDisplay);
                            var newDiv = $('<div>').appendTo(container);
                            node.appendTo(newDiv);

                            DataTable.ext.internal._fnBindAction(
                                node, { action: button }, clickHandler
                            );

                            counter++;
                        }
                    }
                }
            };

            attach($(host).empty(), buttons);
        }
    }
});

class ExportTool extends React.Component {
    render() {
        var endpoint = 'https://us-central1-scss-elth.cloudfunctions.net/exportSeries';
        var exporter = () => {
            var series = this.series.state.value || 'CD';
            var quizzes = parseFloat(this.quizzes.state.value) || 10;
            var total = parseFloat(this.total.state.value) || 10;

            console.log({ series, quizzes, total });
            window.open(endpoint + '?series=' + series + '&quizzes=' + quizzes + '&total=' + total);
        };

        return <div className="export-tool">
            <h2>Exportă catalog serie</h2>
            <p>
                <Input ref={(ref) => this.series = ref} type="text" placeholder="Serie" value="CD" />
                <Input ref={(ref) => this.quizzes = ref} type="text" placeholder="Număr quiz-uri susținute" value="10" />
                <Input ref={(ref) => this.total = ref} type="text" placeholder="Punctaj total pentru quiz-urile susținute" value="10" />
                <Button flat={true} onClick={exporter}>Exportă</Button>
            </p>
        </div>;
    }
}

class Catalog extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        location: PropTypes.object,
        database: PropTypes.object
    }
    constructor() {
        super();

        this.state = {
            students: []
        };
        this.dataTable = this.dataTable.bind(this);
        this.onChange = this.onChange.bind(this);
    }
    shouldComponentUpdate() {
        return false;
    }
    dataTable() {
        var self = this;
        this.table = $('#table').DataTable({
            bPaginate: true,
            bLengthChange: true,
            bFilter: true,
            bSort: true,
            bInfo: true,
            bAutoWidth: true,
            bResponsive: true,
            language: {
                sProcessing: 'Procesează...',
                sLengthMenu: 'Afișează _MENU_ înregistrări pe pagină',
                sZeroRecords: 'Nu am găsit nimic - ne pare rău',
                sInfo: 'Afișate de la _START_ la _END_ din _TOTAL_ înregistrări',
                sInfoEmpty: 'Afișate de la 0 la 0 din 0 Înregistrări',
                sInfoFiltered: '(filtrate dintr-un total de _MAX_ înregistrări)',
                sInfoPostFix: '',
                sSearch: 'Caută:',
                sUrl: '',
                oPaginate: {
                    sFirst: 'Prima',
                    sPrevious: 'Precedenta',
                    sNext: 'Următoarea',
                    sLast: 'Ultima'
                }
            },
            aoColumns: [
                null,
                null,
                null,
                null,
                null,
                { 'sType': 'date-euro' },
                null,
                null,
                null
            ],
            lengthMenu: [10, 20, 30, 50, 100],
            columnDefs: [
                { 'className': 'dt-center', 'targets': [5] },
                { 'className': 'dt-right', 'targets': [1, 6] }
            ],
            order: [5, 'desc'],
            search: {
                'caseInsensitive': false
            },
            searchDelay: 250,
            initComplete: function () {
                // move search inputs up
                var r = $('#table tfoot tr');
                $('#table thead').prepend(r);
                $('#search_0').css('text-align', 'center');

                // move "Show x per page" on bottom
                $('#table_length').insertBefore($('#table_info'));
                $('#table_info').remove();

                var temp = $('#temp');
                var tableLength = $('#table_length');
                var oldSelect = tableLength.find('select');

                var selectEvents = $._data(oldSelect[0], 'events');
                var newSelect = temp.find('#selectParent > div').detach();
                self.oldOnChange = selectEvents.change[0].handler;
                newSelect.find('select').on('change', selectEvents.change[0].handler);
                newSelect.insertBefore(oldSelect);
                oldSelect.remove();
                self.onChange('30');

                temp.remove();
            }
        });

        var table = this.table;
        // Apply the search
        table.columns().eq(0).each(function (colIdx) {
            $('input', table.column(colIdx).footer()).on('keyup change', function () {
                var value = translate(this.value);

                table
                    .column(colIdx)
                    .search(value)
                    .draw();
            });
        });
    }
    onChange(value) {
        $('.materialSelect').attr('value', value);
        if (this.oldOnChange) {
            this.oldOnChange.call({
                type: 'option',
                nodeName: '.materialSelect',
                getAttribute: () => value
            });
        }
    }
    async componentDidMount() {
        var database = this.props.database;

        var tests, students, quizzes;
        await Promise.all([
            (async () => {
                var temp = await database.ref('/timeline').once('value');
                tests = temp.val();
            })(),
            (async () => {
                var temp = await database.ref('/users').once('value');
                students = temp.val();
            })(),
            (async () => {
                var temp = await database.ref('/quizzes').once('value');
                quizzes = temp.val();
            })(),
        ]);
        //For each student search for quizzes 
        Object.keys(students).forEach((studentID) => {
            var stufToAdd = {
                id: studentID,
                name: students[studentID].name || students[studentID],
                quizzes: [],
                series: students[studentID].series || 'fără serie',
                group: students[studentID].group || 'fără grupă'
            };
            stufToAdd.series = stufToAdd.series.toUpperCase();
            var quizzes = students[studentID].quizzes;

            if (quizzes)
                quizzes.forEach((quiz) => {
                    var result, start, stop;
                    if (!tests[quiz]) {
                        return;
                    }
                    if (!tests[quiz].results || !tests[quiz].results[studentID]) {
                        result = '0?';
                        start = 0;
                        stop = 0;
                    }
                    else {
                        result = tests[quiz].results[studentID].total;
                        start = tests[quiz].answers[studentID].start;
                        stop = tests[quiz].answers[studentID].stop;
                    }

                    var toAdd = {
                        id: tests[quiz].quiz,
                        results: result,
                        start: start,
                        duration: stop - start
                    };
                    stufToAdd.quizzes.push(toAdd);
                });

            this.state.students.push(stufToAdd);
        });

        this.quizzes = quizzes;
        this.forceUpdate(this.dataTable);
    }
    render() {
        if (!this.state.students.length) {
            return (
                <div className="Loading">Loading</div>
            );
        }

        var options = [
            10,
            20,
            {
                text: '30',
                selected: true
            },
            50,
            100
        ];

        return (
            <div className="catalog">
                <div id="temp">
                    <div id="selectParent">
                        <Select inline={true} width={100} height={30} options={options} onChange={this.onChange} />
                    </div>
                </div>
                <div id="buttonTemp" className="hidden">
                    <div id="buttonParent">
                        <Button flat></Button>
                    </div>
                </div>
                <table id="table">
                    <thead>
                        <tr className="headers">
                            <th className="name">Nume</th>
                            <th className="group">Grupă</th>
                            <th className="series">Serie</th>
                            <th className="quizName">Nume quiz</th>
                            <th className="quizType">Tip quiz</th>
                            <th className="date">Dată</th>
                            <th className="duration">Durată</th>
                            <th className="grade">Notă</th>
                            <th className="noSort buttonsHeader"></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr className="inputs">
                            <th className="name"><Input type="text" placeholder="Nume" /></th>
                            <th className="group"><Input type="text" placeholder="Grupă" /></th>
                            <th className="series"><Input type="text" placeholder="Serie" /></th>
                            <th className="quizName"><Input type="text" placeholder="Nume quiz" /></th>
                            <th className="quizType"><Input type="text" placeholder="Tip quiz" /></th>
                            <th className="date"><Input type="text" placeholder="Dată" /></th>
                            <th className="duration"><Input type="text" placeholder="Durată" /></th>
                            <th className="grade"><Input type="text" placeholder="Notă" /></th>
                            <th className="noSort buttonsHeader"></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        {
                            this.state.students.map((student, studentIndex) => {
                                return student.quizzes.map((quiz, quizIndex) => {
                                    if (!this.quizzes[quiz.id]) {
                                        return;
                                    }

                                    return (
                                        <tr className="data" key={`${studentIndex}-${quizIndex}`}>
                                            <td>{translate(student.name)}</td>
                                            <td>{translate(student.group)}</td>
                                            <td>{translate(student.series)}</td>
                                            <td>{translate(this.quizzes[quiz.id].title)}</td>
                                            <td>{translate(this.quizzes[quiz.id].type)}</td>
                                            <td>{moment.utc(quiz.start).format('DD-MM-YYYY \n HH:mm')}</td>
                                            <td>{moment(quiz.duration).format('mm:ss')}</td>
                                            <td>{quiz.results}</td>
                                            <td className="buttons"><div title="Vezi detalii"><Button flat><i className="fa fa-eye" /></Button></div></td>
                                        </tr>
                                    );
                                });
                            })
                        }
                    </tbody>
                </table>
                <ExportTool />
            </div>
        );
    }
}

export default Catalog;