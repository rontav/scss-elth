import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { Input, Button, Select } from 'material-react';
import Checkbox from 'material-ui/Checkbox';

import '../scss/results.scss';
import '../scss/catalog.scss';
import '../scss/startQuiz.scss';

function translate(s) {
    if (typeof s != 'string' || !s) {
        return s;
    }
    var translateRegex = /[ăâîșțÂĂÎȘȚ]/g;
    var translate = {
        'ă': 'a', 'â': 'a', 'î': 'i', 'ș': 's', 'ț': 't',
        'Ă': 'A', 'Â': 'A', 'Î': 'I', 'Ș': 'S', 'Ț': 'T'
    };
    return (s.replace(translateRegex, function (match) {
        return translate[match];
    }));
}

var DataTable = $.fn.DataTable;
$.extend(true, DataTable.ext.renderer, {
    pageButton: {
        _: function (settings, host, idx, buttons, page, pages) {
            var classes = settings.oClasses;
            var lang = settings.oLanguage.oPaginate;
            var aria = settings.oLanguage.oAria.paginate || {};
            var btnDisplay, btnClass, counter = 0;

            var attach = function (container, buttons) {
                var i, ien, node, button;
                var clickHandler = function (e) {
                    DataTable.ext.internal._fnPageChange(settings, e.data.action, true);
                };

                var btn = $('#buttonParent > div');
                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if ($.isArray(button)) {
                        var inner = $('<' + (button.DT_el || 'div') + '/>').appendTo(container);
                        attach(inner, button);
                    }
                    else {
                        btnDisplay = null;
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                container.append('<span class="ellipsis">&#x2026;</span>');
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ?
                                    '' : ' ' + classes.sPageButtonDisabled);
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                    classes.sPageButtonActive : '';
                                break;
                        }

                        if (btnDisplay !== null) {
                            node = btn.clone(true, true);

                            node.addClass(classes.sPageButton + ' ' + btnClass);
                            node.attr('aria-controls', settings.sTableId);
                            node.attr('aria-label', aria[button]);
                            node.attr('data-dt-idx', counter);
                            node.attr('tabindex', settings.iTabIndex);
                            node.attr('id', idx === 0 && typeof button === 'string' ?
                                settings.sTableId + '_' + button :
                                null);
                            node[0].insertAdjacentHTML('beforeend', btnDisplay);
                            var newDiv = $('<div>').appendTo(container);
                            node.appendTo(newDiv);

                            DataTable.ext.internal._fnBindAction(
                                node, { action: button }, clickHandler
                            );

                            counter++;
                        }
                    }
                }
            };

            attach($(host).empty(), buttons);
        }
    }
});

class StartQuiz extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        location: PropTypes.object,
        database: PropTypes.object,
        params: PropTypes.any
    }
    constructor() {
        super();
        this.state = {
            results: [],
            rerender: false,
            loaded: false,
            checked: []
        };
        this.dataTable = this.dataTable.bind(this);
        this.onChange = this.onChange.bind(this);
        this.tableLength = 30;
    }
    shouldComponentUpdate() {
        return false;
    }
    dataTable() {
        var self = this;

        self.table = $('#table').DataTable({
            bPaginate: true,
            bLengthChange: true,
            bFilter: true,
            bSort: true,
            bInfo: true,
            bAutoWidth: true,
            bResponsive: true,
            language: {
                sProcessing: 'Procesează...',
                sLengthMenu: 'Afișează _MENU_ înregistrări pe pagină',
                sZeroRecords: 'Nu am găsit nimic - ne pare rău',
                sInfo: 'Afișate de la _START_ la _END_ din _TOTAL_ înregistrări',
                sInfoEmpty: 'Afișate de la 0 la 0 din 0 Înregistrări',
                sInfoFiltered: '(filtrate dintr-un total de _MAX_ înregistrări)',
                sInfoPostFix: '',
                sSearch: 'Caută:',
                sUrl: '',
                oPaginate: {
                    sFirst: 'Prima',
                    sPrevious: 'Precedenta',
                    sNext: 'Următoarea',
                    sLast: 'Ultima'
                }
            },
            aoColumns: [
                null,
                null,
                null
            ],
            lengthMenu: [10, 20, 30, 50, 100],
            columnDefs: [
                { 'className': 'dt-center', 'targets': [0] }
            ],
            order: [1, 'asc'],
            search: {
                'caseInsensitive': false
            },
            searchDelay: 250,
            initComplete: function () {
                // move search inputs up
                var r = $('#table tfoot tr');
                $('#table thead').prepend(r);
                $('#search_0').css('text-align', 'center');

                // move "Show x per page" on bottom
                $('#table_length').insertBefore($('#table_info'));
                $('#table_info').remove();

                var temp = $('#temp');
                var tableLength = $('#table_length');
                var oldSelect = tableLength.find('select');

                var selectEvents = $._data(oldSelect[0], 'events');
                var newSelect = temp.find('#selectParent > div').detach();
                self.oldOnChange = selectEvents.change[0].handler;
                newSelect.find('select').on('change', selectEvents.change[0].handler);
                newSelect.insertBefore(oldSelect);
                oldSelect.remove();
                self.onChange('30');

                temp.remove();
            }
        });

        // Apply the search
        self.table.columns().eq(0).each((colIdx) => {
            $('input', self.table.column(colIdx).footer()).on('keyup change', function () {
                var value = translate(this.value);

                self.table
                    .column(colIdx)
                    .search(value)
                    .draw();

                setTimeout(() => {
                    var all = true;
                    $('tbody tr').each((index, elem) => {
                        var studentKey = $(elem).attr('data-id');
                        if (!self.state.checked[studentKey]) {
                            all = false;
                        }
                    });

                    self.masterChecked = all;
                    self.forceUpdate();
                });
            });
        });
    }
    onClick(studentID, e) {
        e.nativeEvent.stopImmediatePropagation();
        if (studentID == 'all') {
            var selected = !this.masterChecked;
            $('tbody tr').each((index, elem) => {
                var studentKey = $(elem).attr('data-id');
                // this.refs['checkbox-' + studentKey].state.switched = selected;
                this.state.checked[studentKey] = selected;
            });
        }
        else {
            this.state.checked[studentID] = !this.state.checked[studentID];
        }
        var all = true;
        $('tbody tr').each((index, elem) => {
            var studentKey = $(elem).attr('data-id');
            if (!this.state.checked[studentKey]) {
                all = false;
            }
        });
        this.masterChecked = all;
        this.forceUpdate();
    }
    onChange(value) {
        this.tableLength = value;
        $('.materialSelect').attr('value', value);
        if (this.oldOnChange) {
            this.oldOnChange.call({
                type: 'option',
                nodeName: '.materialSelect',
                getAttribute: () => value
            });
        }
    }
    componentDidMount() {
        this.props.database.ref('/users/').once('value', (students) => {
            students = students.val();
            this.students = students;
            this.state.loaded = true;

            this.forceUpdate(this.dataTable);
        });
    }
    componentWillUnmount() {
        if (this.timelineRef) {
            this.timelineRef.off();
        }
    }
    render() {
        if (!this.state.loaded) {
            return (
                <div className="Loading">Loading</div>
            );
        }
        var options = [
            10,
            20,
            {
                text: '30',
                selected: true
            },
            50,
            100
        ];

        return (
            <div>
                <div id="temp">
                    <div id="selectParent">
                        <Select inline={true} width={100} height={30} options={options} onChange={this.onChange} />
                    </div>
                </div>
                <div id="buttonTemp" className="hidden">
                    <div id="buttonParent">
                        <Button flat></Button>
                    </div>
                </div>
                <table id="table">
                    <thead>
                        <tr className="headers">
                            <th className="noSort buttonsHeader">
                                <Checkbox
                                    onClick={this.onClick.bind(this, 'all')}
                                    ref="master"
                                    checked={this.masterChecked}
                                    disableTouchRipple={true}
                                    className="checkbox"
                                    iconStyle={{ fill: '#3F51B5' }}
                                />
                            </th>
                            <th className="Student">Nume</th>
                            <th className="Grupă">Grupă</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr className="inputs">
                            <th className="noSort buttonsHeader"></th>
                            <th className="studentName"><Input type="text" placeholder="Nume" /></th>
                            <th className="studentGroup"><Input type="text" placeholder="Grupă" /></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        {
                            Object.keys(this.students).map((studentKey) => {
                                var student = this.students[studentKey];
                                return (
                                    <tr className="data" key={studentKey} data-id={studentKey}>
                                        <td onClick={(e) => {
                                            e.nativeEvent.stopImmediatePropagation();
                                            return false;
                                        }} className="forCheckbox"><Checkbox
                                                ref={'checkbox-' + studentKey}
                                                onClick={this.onClick.bind(this, studentKey)}
                                                disableTouchRipple={true}
                                                checked={this.state.checked[studentKey]}
                                                className="checkbox"
                                                iconStyle={{ fill: '#3F51B5' }}
                                            /></td>
                                        <td>{translate(student.name)}</td>
                                        <td>{translate(student.fullgroup)}</td>
                                    </tr>
                                );
                            })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

class StartQuizWrapper extends React.PureComponent {
    static propTypes = {
        user: PropTypes.object,
        location: PropTypes.object,
        database: PropTypes.object,
        params: PropTypes.any
    }
    constructor() {
        super();

        this.state = {
            results: [],
            rerender: false,
            loaded: false,
            checked: []
        };

        this.startQuiz = this.startQuiz.bind(this);
        this.writeTimeline = this.writeTimeline.bind(this);
    }
    startQuiz() {
        this.state.hasError = '';
        var time = this.refs.minutes.state.value;
        if (parseFloat(time) <= 0 || isNaN(parseFloat(time))) {
            this.setState({
                hasError: 'Limita de timp trebuie să fie un număr pozitiv'
            });
            window.scrollToTop();
            return;
        }

        var students = Object.keys(this.table.state.checked).map(key => {
            return this.table.state.checked[key] ? key : undefined;
        }).filter(e => e);

        if (!students || !students.length) {
            this.setState({
                hasError: 'Trebuie selectat cel puțin un student din lista de mai jos'
            });
            window.scrollToTop();
            return;
        }

        this.writeTimeline(this.props.params.id, students, time);
    }
    writeTimeline(quizID, studentsArray, time) {
        var db = this.props.database;

        var date = new Date().getTime();
        var session = {
            active: true,
            quiz: quizID,
            start: date,
            stop: date + (time * 1000 * 60 + 30 * 1000),
            students: studentsArray
        };
        var ref = db.ref('/timeline').push();

        studentsArray.forEach(studentKey => {
            var toInsertKey = this.table.students[studentKey].quizzes ? this.table.students[studentKey].quizzes.length : 0;
            db.ref('/users/' + studentKey).child('quizzes').child(toInsertKey).set(ref.key);
        });

        // ref.key if need sessionID

        ref.set(session, (err) => {
            this.state.started = true;
            this.forceUpdate();
            if (err) {
                console.log(err);
            }
        });
    }
    render() {
        if (this.state.started) {
            return (
                <Redirect to={{
                    pathname: '/',
                    state: { from: this.props.location }
                }} />
            );
        }

        return (
            <div className="catalog elev">
                {
                    this.state.hasError ?
                        <div className="errorMessage">{this.state.hasError}</div>
                        : ''
                }
                <StartQuiz {...this.props} ref={(table) => this.table = table} />
                <div className="startQuizWrapper">
                    <Input type="text" placeholder="Limită de timp (minute)" ref="minutes" />
                    <Button raised onClick={this.startQuiz}>Lansare quiz</Button>
                </div>
            </div>
        );
    }
}

export default StartQuizWrapper;