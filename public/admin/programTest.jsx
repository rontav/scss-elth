import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Link } from 'react-router-dom';
import moment from 'moment';
import { Input, Button, Select } from 'material-react';

import '../scss/catalog.scss';


class Program extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        location: PropTypes.object,
        database: PropTypes.object
    }
    constructor() {
        super();
        this.state = {
            quizzes: {}
        };
    }
    componentDidMount() {
        var quizRef = this.props.database.ref('/quizzes/').orderByChild('type').equalTo('Test de seminar');
        quizRef.on('value', (quizzes) => {
            this.state.quizzes = quizzes.val();
            this.setState({
                quizzes: quizzes.val()
            });
        });
    }
    render() {
        if (!this.state.quizzes || !Object.keys(this.state.quizzes).length) {
            return <div className="Loading">Loading</div>;
        }
        return (
            <div className="admin">
                <div className="quiz-list">
                    {this.state.quizzes ? Object.keys(this.state.quizzes).map((quiz, index) => {
                        if (this.state.quizzes[quiz].hidden) {
                            return '';
                        }
                        return <div className='quiz-card' key={index}>
                            <div className="quiz-elem title">{this.state.quizzes[quiz].title}</div>
                            <div className="quiz-elem type">{this.state.quizzes[quiz].type} </div>
                            <Link to={`/admin/startquiz/${quiz}`}><Button flat className="edit"><i className="fa fa-clock-o" aria-hidden="true"></i></Button></Link>
                        </div>;
                    }) : ''}
                </div>
            </div>
        );

    }
}
export default Program;