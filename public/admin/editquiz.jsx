import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Redirect } from 'react-router-dom';
import { Input, Button, Select } from 'material-react';
import Checkbox from 'material-ui/Checkbox';

import { debounce, cloneDeep, sortBy } from 'lodash';
var clone = cloneDeep;

import renderTextLatex from '../components/renderTextLatex.js';

import '../scss/editQuiz.scss';

class CheckboxAnswer extends React.Component {
    static propTypes = {
        answerIndex: PropTypes.number,
        answer: PropTypes.object,
        removeAnswer: PropTypes.func,
        onChange: PropTypes.func
    }
    constructor(props) {
        super();
        this.state = {};
        if (props.answer) {
            Object.keys(props.answer).forEach(propName => {
                this.state[propName] = props.answer[propName];
            });
        }
        this.handleInput = this.handleInput.bind(this);
    }
    handleChange(attr, value, checkboxValue) {
        if (typeof value == 'string') {
            this.setState({
                [attr]: value
            }, () => {
                this.props.onChange(this.state);
            });
        }
        else {
            if (!this.state.title) {
                return;
            }
            this.setState({
                [attr]: checkboxValue
            }, () => {
                this.props.onChange(this.state);
            });
        }
    }
    handleInput(newValue) {
        if (!this.state.title && newValue) {
            this.setState({
                title: newValue
            }, () => {
                this.props.onChange(this.state);
            });
        }
    }
    componentWillReceiveProps(newProps) {
        if (newProps.answer) {
            this.setState(newProps.answer);
        }
    }
    render() {
        var { checked, title } = this.state;

        return (
            <div className="answer">
                <div className="checkbox">
                    <Checkbox
                        disableTouchRipple={true}
                        iconStyle={{ fill: '#3F51B5' }}
                        checked={checked}
                        onCheck={this.handleChange.bind(this, 'checked')}
                    />
                </div>
                <div className="text"><Input type="text" placeholder="Răspuns" value={title} tabIndex={this.props.answerIndex > 0 ? this.props.answerIndex : undefined} onInput={this.handleInput} onChange={this.handleChange.bind(this, 'title')} /></div>
                <Button flat className="delete" onClick={this.props.removeAnswer}><i className="fa fa-trash" /></Button>
            </div>
        );
    }
}
var TextAnswer = CheckboxAnswer;

class SelectBoxAnswer extends React.Component {
    static propTypes = {
        answer: PropTypes.object,
        removeAnswer: PropTypes.func,
        onChange: PropTypes.func
    }
    constructor(props) {
        super();
        this.state = {};
        if (props.answer) {
            Object.keys(props.answer).forEach(propName => {
                this.state[propName] = props.answer[propName];
            });
        }
        this.handleChange = this.handleChange.bind(this);
    }
    componentWillReceiveProps(newProps) {
        if (newProps.answer) {
            delete this.state;
            this.state = {};
            Object.keys(newProps.answer).forEach(propName => {
                this.state[propName] = newProps.answer[propName];
            });
        }
    }
    handleChange(value) {
        this.setState({
            title: value
        }, () => {
            this.props.onChange(this.state);
        });
    }
    handleCheckboxChange(answerIndex, answer) {
        this.setState((oldState) => {
            if (answer.checked) {
                oldState.answers.forEach((answer, answerIndex) => {
                    oldState.answers[answerIndex].checked = false;
                });
            }
            oldState.answers[answerIndex] = clone(answer);
            return oldState;
        }, () => {
            this.props.onChange(this.state);
        });
    }
    removeCheckbox(index) {
        this.setState((oldState) => {
            oldState.answers.splice(index, 1);
            return oldState;
        }, () => {
            this.props.onChange(this.state);
        });
    }
    render() {
        var { answers, title } = this.state;

        return (
            <div className="selectAnswerWrapper">
                <Input type="text" placeholder="Enunț select" value={title} onChange={this.handleChange} />
                <Button flat className="delete" onClick={this.props.removeAnswer}><i className="fa fa-trash" /></Button>
                <div className="selectAnswers">
                    {
                        answers && answers.map((answer, answerIndex) => {
                            return <CheckboxAnswer answer={answer} removeAnswer={this.removeCheckbox.bind(this, answerIndex)} onChange={this.handleCheckboxChange.bind(this, answerIndex)} key={answerIndex} />;
                        })
                    }
                </div>
            </div>
        );
    }
}

class FirebaseImage extends React.Component {
    static propTypes = {
        src: PropTypes.string,
        onClose: PropTypes.func
    }
    render() {
        var style = {
            backgroundImage: `url('${this.props.src}')`
        };

        return (
            <div className="firebaseImage">
                <div className="closeButton"><Button raised className="iconBtn close" onClick={this.props.onClose} /></div>
                <a href={this.props.src} target="_blank" className="background" style={style} />
            </div>
        );
    }
}

class EditQuiz extends React.Component {
    static propTypes = {
        params: PropTypes.object,
        user: PropTypes.object,
        location: PropTypes.object,
        database: PropTypes.object,
        history: PropTypes.object,
        storage: PropTypes.object
    }
    constructor(props) {
        super(props);

        this.state = {
            quiz: {
                questions: []
            },
            progress: {},
            saved: false,
            quest: false,
            create: false
        };

        this.updateQuiz = this.updateQuiz.bind(this);
        this.quizTitleChange = this.quizTitleChange.bind(this);
        this.quizTypeChange = this.quizTypeChange.bind(this);
        this.questionTitleChangeDebounced = debounce(this.questionTitleChangeDebounced, 500);
    }
    async updateQuiz() {
        if (this.refs && this.refs.title && this.refs.select) {
            if (this.refs.title.state.value && this.refs.select.state.selected.text != 'Tipul de test') {

                var questions = clone(this.state.quiz.questions);
                questions = questions.filter(question => {
                    question.points = question.points || 1;

                    delete question.titleDebounced;
                    return question.title;
                });
                questions.forEach(question => {
                    question.images = question.images.map(image => image.id);

                    question.answers = question.answers.filter(answer => {
                        if (answer.answers) {
                            answer.answers = answer.answers.filter(answer => answer.title);
                        }
                        return answer.title;
                    });
                });
                if (!this.state.create) {
                    this.quizRef = await this.props.database.ref('quizzes/').child(this.props.params.id);
                    this.quizRef.update({
                        title: this.refs.title.state.value,
                        type: this.refs.select.state.selected.text,
                        questions: questions
                    });
                }
                else {
                    this.quizRef = await this.props.database.ref('quizzes/');
                    var newRef = this.quizRef.push();
                    newRef.set({
                        title: this.refs.title.state.value,
                        type: this.refs.select.state.selected.text,
                        questions: questions
                    }).then(() => {
                        this.props.history.replace(`/admin/edit/${newRef.key}`);
                        this.state.create = false;
                    });
                }
                this.setState({
                    saved: true
                });
                this.updateQuestionNumber();
                console.log('Succes');
            }
        }
    }
    componentDidMount() {
        if (!this.props.params.id) {
            this.setState({
                quiz: {
                    title: '',
                    type: '',
                    questions: []
                },
                create: true
            }, this.updateQuestionNumber);
        }
        else {
            this.quizRef = this.props.database.ref('quizzes/').child(this.props.params.id);

            this.quizRef.once('value', async (quiz) => {
                var imagePromises = [];
                var imageMeta = [];

                quiz = quiz.val();
                if (!quiz) {
                    this.setState({
                        wrongID: true
                    });
                    return;
                }

                quiz.questions = quiz.questions || [];
                quiz.questions.forEach((question, questionIndex) => {
                    question.points = question.points || 1;
                    question.titleDebounced = question.title;
                    question.answers = question.answers || [];

                    if (question.images) {
                        question.images.forEach((image, imageIndex) => {
                            question.images[imageIndex] = {
                                id: image
                            };
                            imagePromises.push(
                                this.props.storage.child(image).getDownloadURL()
                            );
                            imageMeta.push({
                                questionIndex,
                                imageIndex
                            });
                        });
                    }
                });
                var urls = await Promise.all(imagePromises);
                urls.forEach((url, index) => {
                    quiz.questions[imageMeta[index].questionIndex].images[imageMeta[index].imageIndex].url = url;
                });

                this.state.quiz = quiz;
                this.updateQuestionNumber();
            });
        }
    }
    questionTitleChangeDebounced(index, value) {
        this.setState((oldState) => {
            oldState.quiz.questions[index].titleDebounced = value;
            return oldState;
        });
    }
    questionTitleChange(index, value) {
        this.state.quiz.questions[index].title = value;
        this.questionTitleChangeDebounced(index, value);

        this.updateQuestionNumber();
    }
    quizTitleChange(value) {
        this.state.quiz.title = value;
    }
    quizTypeChange(value) {
        this.state.quiz.type = value;
    }
    questionTypeChange(index, value) {
        this.state.quiz.questions[index].type = value;
        this.updateQuestionNumber();
    }
    questionPointsChange(index, value) {
        this.setState((oldState) => {
            oldState.quiz.questions[index].points = value;
            return oldState;
        });
    }
    updateQuestionNumber() {
        var quiz = this.state.quiz;
        if (!quiz || !quiz.questions) {
            return;
        }

        this.setState((oldState) => {
            var quiz = oldState.quiz;

            if (!quiz.questions.length ||
                quiz.questions[quiz.questions.length - 1].title) {
                quiz.questions.push({
                    title: '',
                    type: 'grilă',
                    images: [],
                    answers: []
                });
            }

            quiz.questions.forEach(question => {
                if (!question.answers.length ||
                    question.answers[question.answers.length - 1].title.length) {
                    question.answers.push({
                        title: '',
                        checked: false
                    });
                }
                if (question.type == 'select') {
                    question.answers = question.answers || [];

                    question.answers.forEach((answer, answerIndex) => {
                        if (!answer.title) {
                            return;
                        }

                        if (!answer.answers) {
                            answer.answers = [];
                        }
                        if (!answer.answers.length ||
                            answer.answers[answer.answers.length - 1].title.length) {
                            question.answers[answerIndex].answers.push({
                                title: '',
                                checked: false
                            });
                        }
                    });
                }
            });
            return oldState;
        });
    }
    questionRemove(questionIndex) {
        this.state.quiz.questions.splice(questionIndex, 1);
        this.updateQuestionNumber();
    }
    answerChange(questionIndex, answerIndex, newAnswer) {
        this.state.quiz.questions[questionIndex].answers[answerIndex] = newAnswer;
        this.updateQuestionNumber();
    }
    answerRemove(questionIndex, answerIndex) {
        this.state.quiz.questions[questionIndex].answers.splice(answerIndex, 1);
        this.updateQuestionNumber();
    }
    async addImageToQuestion(index) {
        var file = $(`#question-file-${index}`)[0].files[0];
        if (!file) {
            return;
        }
        this.setState((oldState) => {
            oldState.progress[`question-file-${index}`] = 0;
            return oldState;
        });

        var uid = this.props.database.ref('/images/').push().key;
        var uname = uid + '.' + file.name.substr(file.name.lastIndexOf('.') + 1);
        var imageRef = this.props.storage.child(uname);
        var filePromise = imageRef.put(file);

        filePromise.on('state_changed', (taskSnapshot) => {
            this.setState((oldState) => {
                oldState.progress[`question-file-${index}`] = taskSnapshot.bytesTransferred / taskSnapshot.totalBytes;
                return oldState;
            });
        });

        var snapshot = await filePromise;

        this.setState(async (oldState) => {
            oldState.progress[`question-file-${index}`] = -1;
            if (snapshot.f == 'success') {
                oldState.quiz.questions[index].images.push({
                    id: uname,
                    url: await imageRef.getDownloadURL()
                });
            }
            $(`#question-file-${index}`).val('');
            return oldState;
        });
    }
    deleteImage(index, imageIndex) {
        this.setState((oldState) => {
            oldState.quiz.questions[index].images.splice(imageIndex, 1);

            return oldState;
        });
    }
    render() {
        if (this.state.wrongID) {
            return (
                <Redirect to={{
                    pathname: '/admin/',
                    state: { from: this.props.location }
                }} />
            );
        }
        var options = [
            'Test de pregătire',
            'Test de seminar',
            'Test de curs'
        ];
        if (this.state.quiz && !this.state.create) {
            options.push({
                selected: true,
                text: this.state.quiz.type
            });
            options.splice(options.indexOf(this.state.quiz.type), 1);
        }
        else {
            options.push({
                placeholder: true,
                text: 'Tipul de test'
            });
        }
        if (!this.state.quiz.title && !this.state.create) {
            return <div className="loading">Loading</div>;
        }

        var saveButtonClasses = classnames({
            saved: this.state.saved
        });
        if (this.state.saved) {
            if (this.timeout) {
                clearTimeout(this.timeout);
            }
            this.timeout = setTimeout(() => {
                this.setState({
                    saved: false
                });
            }, 500);
        }

        var tabIndex = 0;
        return (
            <div className="editQuizPage">
                <div className="overlay">
                    <Button raised className={saveButtonClasses} onClick={this.updateQuiz}><i className="fa fa-floppy-o" /></Button>
                </div>
                <div>
                    <div className="quizDetails">
                        <h2 className="pageTitle">Editare Quiz</h2>
                        <Input required={true} placeholder='Titlu quiz' ref='title' onChange={this.quizTitleChange} value={this.state.quiz.title} />
                        <Select inline={true} width={300} required={true} onChange={this.quizTypeChange} options={options} ref='select' />
                    </div>
                    <div className="questions">
                        {
                            this.state.quiz.questions && this.state.quiz.questions.map((question, index) => {
                                question.images = question.images || [];

                                tabIndex++;
                                var questTypeOptions = [
                                    {
                                        placeholder: true,
                                        text: 'Tipul de întrebare'
                                    }
                                ];
                                var types = ['grilă', 'select', 'text'];
                                questTypeOptions.push({
                                    selected: true,
                                    text: question.type
                                });

                                types.forEach(type => {
                                    if (type != question.type) {
                                        questTypeOptions.push({
                                            text: type
                                        });
                                    }
                                });

                                questTypeOptions = sortBy(questTypeOptions, 'text');

                                var progress = -1;
                                if (typeof this.state.progress[`question-file-${index}`] == 'number') {
                                    progress = this.state.progress[`question-file-${index}`];
                                    progress = progress * 100;
                                }
                                var classes = {
                                    uploadButton: classnames({
                                        addImage: true,
                                        loading: progress >= 0
                                    }),
                                    images: classnames({
                                        images: true,
                                        empty: !question.images.length
                                    })
                                };
                                if (progress >= 0) {
                                    classes.uploadButton = classnames(classes.uploadButton, `progress-${Math.round(progress / 10) * 10}`);
                                }

                                return (
                                    <div className="questionWrapper" key={`question-${index}`}>
                                        <div className="question">
                                            <div className="equationLine">
                                                <Input multiline={true} required={true} placeholder='Enunț problemă' value={question.title} onInput={this.questionTitleChange.bind(this, index)} />
                                                <pre className="output">{renderTextLatex(question.titleDebounced)}</pre>
                                            </div>
                                            <div className="header">
                                                <Input placeholder="Punctaj" value={(question.points || '').toString()} required={true} onChange={this.questionPointsChange.bind(this, index)} />
                                                <input id={`question-file-${index}`} type="file" onChange={this.addImageToQuestion.bind(this, index)} />
                                                <Button flat className={classes.uploadButton} onClick={() => $(`#question-file-${index}`).click()} >Adaugă imagine</Button>
                                                <Select inline={true} width={150} required={true} options={questTypeOptions} onChange={this.questionTypeChange.bind(this, index)} />
                                                <Button flat className="delete" onClick={this.questionRemove.bind(this, index)}><i className="fa fa-trash" /></Button>
                                            </div>
                                            <div className={classes.images}>
                                                {
                                                    question.images.map((image, imageIndex) => {
                                                        return (
                                                            <FirebaseImage key={`image-${imageIndex}`} src={image.url} onClose={this.deleteImage.bind(this, index, imageIndex)} />
                                                        );
                                                    })
                                                }
                                            </div>
                                            <div className="answers">
                                                {
                                                    question.answers && question.answers.map((answer, answerIndex) => {
                                                        var removeAnswer = this.answerRemove.bind(this, index, answerIndex);
                                                        var onChange = this.answerChange.bind(this, index, answerIndex);

                                                        if (question.type == 'grilă') {
                                                            tabIndex++;
                                                            return <CheckboxAnswer
                                                                answer={answer}
                                                                onChange={onChange}
                                                                removeAnswer={removeAnswer}
                                                                answerIndex={tabIndex}
                                                                key={`answer-${answerIndex}`} />;
                                                        }
                                                        if (question.type == 'text') {
                                                            tabIndex++;
                                                            return <TextAnswer
                                                                answer={answer}
                                                                onChange={onChange}
                                                                removeAnswer={removeAnswer}
                                                                answerIndex={tabIndex}
                                                                key={`answer-${answerIndex}`} />;
                                                        }
                                                        if (question.type == 'select') {
                                                            return <SelectBoxAnswer
                                                                answer={answer}
                                                                onChange={onChange}
                                                                removeAnswer={removeAnswer}
                                                                key={`answer-${answerIndex}`} />;
                                                        }
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default EditQuiz;
