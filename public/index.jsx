import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom';

import 'reset-css/reset.css';
import './scss/common.scss';

import LoginPage from './components/login.jsx';

import AdminPage from './admin/admin.jsx';
import Catalog from './admin/catalog.jsx';
import EditQuiz from './admin/editquiz.jsx';
import Program from './admin/programTest.jsx';
import StartQuiz from './admin/startQuiz.jsx';

import Results from './components/results.jsx';
import QuizList from './components/quizList.jsx';
import Quiz from './components/quiz.jsx';
import Header from './components/header.jsx';

import firebase from 'firebase';

import Keypad from './components/perseus/keypad.jsx';


var config = {
    apiKey: 'AIzaSyArD5zWbkkCIrr23_jVgpU1zgPKo7wyXSs',
    authDomain: 'scss-elth.firebaseapp.com',
    databaseURL: 'https://scss-elth.firebaseio.com',
    projectId: 'scss-elth',
    storageBucket: 'scss-elth.appspot.com',
    messagingSenderId: '307530544838'
};

firebase.initializeApp(config);

var database = firebase.database();
var imagesStorage = firebase.storage().ref('/quiz-images/');

import smoothscroll from 'smoothscroll';
window.scrollToTop = () => {
    smoothscroll(0, 500, null, document.body);
};

class UserProxy extends React.Component {
    static propTypes = {
        children: PropTypes.node,
        user: PropTypes.object,
        location: PropTypes.any,
        fail: PropTypes.any,
        login: PropTypes.func,
        admin: PropTypes.bool
    }
    render() {
        if (this.props.user.uid == 'guest') {
            return (
                <MuiThemeProvider>
                    <Router>
                        <div>
                            {
                                this.props.fail ? <h1 className="loginFail"> {this.props.fail} </h1>
                                    : ''
                            }
                            <Route path="/" component={() => <LoginPage login={this.props.login} />} />
                        </div>
                    </Router>
                </MuiThemeProvider>
            );
        }
        if (this.props.admin && !this.props.user.admin) {
            return (
                <Redirect to={{
                    pathname: '/',
                    state: { from: this.props.location }
                }} />
            );
        }

        return this.props.children;
    }
}

class App extends React.Component {
    constructor() {
        super();
        var defaultUser = {
            uid: 'guest',
            name: 'guest',
            series: 'guest',
            group: 'guest',
            fullgroup: 'guest',
            admin: false
        };

        this.state = {
            fail: false
        };
        firebase.auth().onAuthStateChanged(async (user) => {
            if (user) {
                this.user = await database.ref('/users/' + user.uid).once('value');
                this.user = this.user.val();
                this.user.uid = user.uid;
                this.user.quizzes = this.user.quizzes || [];
            }
            else {
                this.user = defaultUser;
            }

            this.forceUpdate();
        });
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
    }
    login(provider) {
        if (provider.email != '') {
            firebase.auth().signInWithEmailAndPassword(provider.email, provider.password).catch((error) => {
                console.log(error.code, error.message);
                this.setState({
                    fail: error.message
                });
            });
        }
    }
    logout() {
        firebase.auth().signOut();
        this.forceUpdate();
    }
    render() {
        if (typeof this.user == 'undefined') {
            return (
                <div className="loading">Loading</div>
            );
        }
        var loggedIn = this.user && this.user.uid;
        var title = '';

        if (loggedIn) {
            return (
                <div>
                    <MuiThemeProvider>
                        <div>
                            <Router>
                                <div className="pageWrapper">
                                    <Header ref="header" title={title} user={this.user} logout={this.logout} />
                                    <Switch>
                                        <Route exact path="/" component={() => <QuizList user={this.user} database={database} />} />
                                        <Route exact path="/admin" component={() => {
                                            return (
                                                <UserProxy admin={true} user={this.user} fail={this.state.fail} login={this.login}>
                                                    <AdminPage user={this.user} database={database} />
                                                </UserProxy>
                                            );
                                        }} />
                                        <Route exact path="/admin/startquiz/:id" component={({ match }) => {
                                            return (
                                                <UserProxy admin={true} user={this.user} fail={this.state.fail} login={this.login}>
                                                    <StartQuiz user={this.user} database={database} params={match.params} />
                                                </UserProxy>
                                            );
                                        }} />
                                        <Route exact path="/admin/catalog" component={() => {
                                            return (
                                                <UserProxy admin={true} user={this.user} fail={this.state.fail} login={this.login}>
                                                    <Catalog user={this.user} database={database} />
                                                </UserProxy>
                                            );
                                        }} />
                                        <Route exact path="/admin/edit/:id" component={({ match, history }) => {
                                            return (
                                                <UserProxy admin={true} user={this.user} fail={this.state.fail} login={this.login}>
                                                    <EditQuiz user={this.user} database={database} storage={imagesStorage} params={match.params} history={history} />
                                                </UserProxy>
                                            );
                                        }} />
                                        <Route exact path="/admin/edit" component={({ match, history }) => {
                                            return (
                                                <UserProxy admin={true} user={this.user} fail={this.state.fail} login={this.login}>
                                                    <EditQuiz user={this.user} database={database} storage={imagesStorage} params={match.params} history={history} />
                                                </UserProxy>
                                            );
                                        }} />
                                        <Route exact path="/admin/chooseQuiz" component={() => {
                                            return (
                                                <UserProxy admin={true} user={this.user} fail={this.state.fail} login={this.login}>
                                                    <Program user={this.user} database={database} storage={imagesStorage} />
                                                </UserProxy>
                                            );
                                        }} />
                                        <Route exact path="/quiz/:id" component={({ match }) => {
                                            return (
                                                <UserProxy user={this.user} fail={this.state.fail} login={this.login}>
                                                    <Quiz user={this.user} database={database} storage={imagesStorage} params={match.params} />
                                                </UserProxy>
                                            );
                                        }} />
                                        <Route exact path="/training/:id" component={({ match }) => <Quiz training={true} user={this.user} database={database} storage={imagesStorage} params={match.params} />} />
                                        <Route exact path="/catalog" component={() => {
                                            return (
                                                <UserProxy user={this.user} fail={this.state.fail} login={this.login}>
                                                    <Results user={this.user} database={database} />
                                                </UserProxy>
                                            );
                                        }} />

                                        <Redirect to='/' />
                                    </Switch>
                                </div>
                            </Router>
                        </div>
                    </MuiThemeProvider>
                    <Keypad
                        onElementMounted={(element) => {
                            window.keypad = element;
                            window.keypad.node = ReactDOM.findDOMNode(element);
                        }}
                    />
                </div>
            );
        }

        return (
            <MuiThemeProvider>
                <Router>
                    <div>
                        {this.state.fail ? <h1 className="loginFail"> {this.state.fail} </h1> : ''}
                        <Route path="/" component={() => <LoginPage login={this.login} />} />
                    </div>
                </Router>
            </MuiThemeProvider>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('app')
);
