var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var precss = require('precss');
var path = require('path');

process.browser = true;

var exp = {
    entry: {
        bundle: ['babel-polyfill', './public/index.jsx']
    },
    output: {
        path: path.join(__dirname, 'assets'),
        filename: '[name].js',
        publicPath: '/assets/'
    },
    module: {
        noParse: /node_modules\/lodash\/lodash\.js/,
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            plugins: [
                                'syntax-async-functions',
                                'transform-regenerator',
                                'transform-class-properties',
                                'transform-object-rest-spread'
                            ],
                            presets: [
                                'es2015',
                                'react'
                            ]
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'postcss-loader',
                    'less-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'postcss-loader'
                ]
            },
            {
                test: /\.(png|jpg|gif?)/,
                use: 'url-loader'
            },
            {
                test: /\.(html|eot|svg|ttf|otf|woff(2)?)(\?v=\d+\.\d+\.\d+)?/,
                use: 'file-loader'
            }

        ]
    },
    plugins: [
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer(),
                    precss()
                ]
            }
        }),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
        new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(ro)$/)
    ],
    resolve: {
        extensions: ['.js', '.jsx']
    },
    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM',
        'react-addons-transition-group': 'React.addons.CSSTransitionGroup',
        'kas': 'KAS',
        'underscore': '_',
        'katex': 'katex',
        'jquery': '$'
    },
};

module.exports = exp;