var functions = require('firebase-functions');

var admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

var _ = require('lodash');

var async = require('async');
var KAS = require('./kas');

function getStudents(series, cb) {
    if (!_.isFunction(cb)) return;

    var db = admin.database();

    db.ref('/users').orderByChild('series').equalTo(series).once('value', (snap) => {
        var students = snap.val();

        if (!students || !_.isObject(students)) return cb('no students');

        cb(null, students);
    });
}

function extractQuizSessions(students) {
    var sessions = [];

    _(students).each(student => {
        if (!student || !_.isObject(student) || !student.quizzes || !_.isArray(student.quizzes) || !student.quizzes.length) return;

        _(student.quizzes).each(quiz => {
            if (!quiz || !_.isString(quiz)) return;

            if (sessions.indexOf(quiz) == -1) sessions.push(quiz);
        });
    });

    return sessions;
}

function calculateMax(questions) {
    var max = 0;

    _(questions).each(question => {
        if (!question || !_.isObject(question)) return;

        max += parseFloat(question.points) || 1;
    });

    return max;
}

function extractResultsAndQuiz(sessions, cb) {
    if (!_.isFunction(cb)) return;

    var db = admin.database();
    var sessionObjects = {};

    async.forEachOf(sessions, (value, key, cb) => {
        db.ref(['timeline', value].join('/')).once('value', snap => {
            var session = snap.val();

            // Skip
            if (!session || !_.isObject(session) || !session.quiz || !_.isString(session.quiz) || !session.results || !_.isObject(session.results)) return cb();

            db.ref(['quizzes', session.quiz].join('/')).once('value', snap => {
                var quiz = snap.val();

                // Skip
                if (!quiz || !_.isObject(quiz) || !quiz.title || !_.isString(quiz.title)) return cb();

                session.name = quiz.title;
                session.max = calculateMax(quiz.questions);

                sessionObjects[value] = session;
                cb();
            });

        });
    }, (err) => {
        if (err) {
            console.log('This should never happen');
            return;
        }

        cb(null, sessionObjects);
    });
}

function createTable(sessions, students) {

    var studentData = [];
    var quizzes = [];

    var quizAdded = {};

    _(sessions).each((session) => {

        if (quizAdded[session.quiz]) return;

        var quizName = session.name.split(' ')[0];
        quizzes.push({
            name: quizName,
            results: session.results
        });

        quizAdded[session.quiz] = true;

    });

    _(students).each((student, studentID) => {
        studentData.push({
            name: student.name,
            id: studentID,
            fullgroup: student.fullgroup
        });
    });

    var sorter = (a, b) => {
        return a.name < b.name ? 1 : a.name > b.name ? -1 : 0;
    };
    studentData.sort(sorter);
    quizzes.sort(sorter);

    var quizByName = {};
    _(quizzes).each(quiz => {
        if (quizByName[quiz.name]) {
            quizByName[quiz.name] = _.merge(quizByName[quiz.name], quiz.results);
        }
        else {
            quizByName[quiz.name] = quiz.results;
        }
    });

    var table = [];
    table[0] = ['Nume', 'Grupă'];
    _(Object.keys(quizByName)).each(name => {
        table[0].push(name);
    });

    _(studentData).each(student => {
        var row = [];
        row.push(student.name);
        row.push(student.fullgroup);

        _(Object.keys(quizByName)).each(quiz => {
            var mark = 0;

            if (_.isObject(quizByName[quiz][student.id])) {
                mark = quizByName[quiz][student.id].total / quizByName[quiz][student.id].max || 0;
            }
            else if (_.isNumber(quizByName[quiz][student.id])) {
                mark = quizByName[quiz][student.id];
            }

            row.push(mark || 0);
        });

        table.push(row);
    });

    return table;
}

function calculateExport(table, options) {
    _(table).each((row, rowID) => {
        if (!rowID) return;

        var total = 0;
        _(row).each((column, columnID) => {
            if (parseInt(columnID) < 2) return;

            var current = Math.floor(column * options.quizValue * 100) / 100;
            total += current;

            table[rowID][columnID] = current;
        });

        table[rowID].push(total);
    });

    table[0].push('Notă finală');

    return table;
}

function createCSV(table) {
    var raw = '';

    _(table).each(row => {
        _(row).each(column => {
            raw += column + ',';
        });

        raw = raw.substr(raw, raw.length -1);
        raw += '\n';
    });

    return raw;
}

exports.exportSeries = functions.https.onRequest((req, res) => {
    var data = req.query;

    if (!_.isString(data.series)) return res.json({ error: 400, info: 'no series' });
    var total = data.total || 10;
    var quizzes = data.quizzes || 10;
    var quizValue = total / quizzes;

    var exportOptions = {
        total, quizzes, quizValue
    };

    console.log('Exporting: Getting students');
    getStudents(data.series, (err, students) => {
        if (err) {
            return res.json({ error: 500, info: err });
        }

        // Debugging
        console.log('Students:', JSON.stringify(students));

        var sessions = extractQuizSessions(students);
        if (!sessions || !_.isArray(sessions)) {
            return res.json({ error: 404, info: 'no sessions' });
        }

        // Debugging
        console.log('Sessions:', JSON.stringify(sessions));

        console.log('Exporting: Extracting results and quizzes');
        extractResultsAndQuiz(sessions, (err, sessions) => {
            if (err) {
                return res.json({ error: 500, info: 'unknown' });
            }
            
            // Debuggin
            console.log('Sessions Dats:', JSON.stringify(sessions));

            console.log('Exporting: Creating table');
            var table = createTable(sessions, students);
            table = calculateExport(table, exportOptions);

            var date = new Date();
            var filename = [date.getFullYear(), (date.getMonth() + 1), date.getDate()].join('-');
            filename += '_' + [date.getUTCHours(), date.getUTCMinutes()].join('-') + '_Seria_' + data.series + '.csv';

            var csv = createCSV(table);
            res.set('Content-Type', 'text/csv');
            res.set('Content-Disposition', 'attachment; filename="' + filename + '"');
            res.send(csv);
        });
    });
});

function extractAnswers(questions, stringCheckbox, onlyAnswers) {
    var answers = {};

    _(questions).each((question, id) => {
        if (!question) return;

        if (_.isArray(question.answers) || _.isObject(question.answers)) {
            var validAnswers = [];

            if (question.type == 'select') {
                // Check if it has sub questions
                if (_.isArray(question.answers)) {
                    // Extract answers from the sub questions with string mode
                    validAnswers = extractAnswers(question.answers, true, true);
                }
            }

            _(question.answers).each((answer, answer_id) => {
                if (!answer) return;

                // Input question has priority
                if (question.type == 'text') {
                    validAnswers.push(answer.title);
                    return;
                }

                if (answer.checked) {
                    if (stringCheckbox)
                        return validAnswers.push(answer.title);

                    validAnswers.push(answer_id);
                }
            });

            answers[id] = {
                answers: validAnswers,
                points: parseFloat(question.points) || 1,
                type: question.type
            };
        }
        else {
            answers[id] = {
                answers: questions.answers,
                points: parseFloat(question.points) || 1,
                type: question.type
            };
        }

    });

    if (onlyAnswers) {
        var extracted = [];

        _(answers).each((answer) => {
            if (!answer || !_.isObject(answer)) return;

            extracted.push(answer.answers);
        });

        return extracted;
    }

    return answers;
}

function compareAnswer(valid, answer, type) {
    if (!valid) return true;
    if (!answer) return false;

    // Used variables
    var need = 0, have = 0;

    if (_.isArray(valid)) {
        need = valid.length;

        // Input type question has priority
        if (type == 'text') {
            // Student's answer should be string
            if (_.isString(answer)) {
                // Create expression from student's answer
                var studentAnswer = KAS.parse(answer);
                
                var goodAnswer = false;
                _(valid).each((correctForm) => {
                    // correctForm must be string
                    if (!correctForm || !_.isString(correctForm)) return;

                    var correctAnswer = KAS.parse(correctForm);

                    if (!studentAnswer.parsed || !correctAnswer.parsed) {
                        return;
                    }
                    var temp = KAS.compare(studentAnswer.expr, correctAnswer.expr);

                    if (temp.equal) {
                        goodAnswer = true;
                    }
                });

                if (goodAnswer)
                    return true;
            }
        }

        // Check for select
        if (type == 'select') {
            // Make sure we can check this
            if (!valid || !answer || !_.isArray(answer) || !_.isArray(valid)) return false;
            have = 0;
            need = 0;

            // Compare every sub-answer
            _(valid).each((valid, key) => {
                need ++;

                var current = compareAnswer(valid, answer[key], null);

                have +=+ current;
            });

            return Math.floor(have / need * 100) / 100;
        }

        // Checking checkbox and others
        if (_.isArray(answer)) {
            have = 0;

            // Check only the first answers given
            if (answer.length > need) {
                answer.splice(need);
            }

            // Iterate student's answers and check if is in the valid array
            _(answer).each(piece => {
                if (valid.indexOf(piece) != -1) have++;
            });

            return Math.floor(have / need * 100) / 100;
        }
        else if (valid.indexOf(answer) != -1)
            return Math.floor(1 / need * 100) / 100;

        return false;
    }

    // Strange case scenario
    if (type == 'text') {
        // It means valid is not array
        if (_.isString(valid)) {
            var correctAnswer = KAS.parse(valid);

            // Answer must be string too
            if (_.isString(answer)) {
                var studentAns = KAS.parse(answer);

                if (!studentAns.parsed || !correctAnswer.parsed) {
                    return false;
                }
                var temp = KAS.compare(studentAns.expr, correctAnswer.expr);

                return temp.equal;
            }

        }
    }
    return valid == answer;
}

function calculateResult(valid, user) {
    var total = 0, max = 0, answered = 0, questions = Object.keys(valid).length;

    _(valid).each((question, id) => {
        var found = -1;

        _(user).each((answer, idx) => {
            if (!answer || answer.id != id) return;

            found = idx;
            var result = compareAnswer(question.answers, answer.answer, question.type);
            result = isNaN(result) ? 0 : result;

            // Debugging
            // console.log('Valid:', JSON.stringify(question.answers), 'Student:', JSON.stringify(answer.answer), 'Type:', question.type, 'Result:', result);

            // Beware the magic code
            answered +=+ result;
            total += (+result) * question.points;
            max += question.points;
        });

        if (found != -1) user.splice(found, 1);
    });

    return {total, max, answered, questions};
}

function finishQuiz(sessionID, session) {
    // Check the session id
    if (!sessionID || !_.isString(sessionID)) {
        return console.log('Correcting session triggered without sessionID');
    }
    var triggered = ['Triggered while correcting:', sessionID].join(' ');

    // Check if session has answers
    if (!session.answers || !_.isObject(session.answers)) {
        return console.log('This quiz session has no answers!', triggered);
    }

    // Check if session ended
    if (!session.active) {
        return console.log('This quiz session ended already!', triggered);
    }
    
    var db = admin.database();
    db.ref(['quizzes', session.quiz, 'questions'].join('/')).once('value', (snap) => {
        var questions = snap.val();

        // Something is strange with the quiz
        if (!questions || (!_.isObject(questions) && !_.isArray(questions))) {
            return console.log('This quiz has no questions.', triggered);
        }

        // Extract valid answers
        var valid = extractAnswers(questions);

        // Debugging
        // console.log('Questions:', JSON.stringify(valid));
        
        // We will store results in this object
        var results = {};

        // Iterating students answers
        _(session.answers).each((submitted, userID) => {
            // Calculate the result for this set of answers
            
            // Check if has valid answers object/array
            if (!_.isObject(submitted) || (!_.isObject(submitted.answers) && !_.isArray(submitted.answers))) {
                results[userID] = 0;
                return;
            }

            // Calculate results for user | clone answers so they won't get removed from DB
            var result = calculateResult(valid, _.cloneDeep(submitted.answers));

            // Put student mark (total) in results
            results[userID] = {total: result.total, max: result.max};
        });

        // Update the session with results and change status
        session.active = false;
        session.results = results;

        // Create some stats
        var stats = {
            students: session.students.length || 0,
            results: Object.keys(results).length || 0
        };

        // Write session in DB
        db.ref(['timeline', sessionID].join('/')).set(session, (err) => {
            // If we have an error log it
            if (_.isError(err)) {
                return console.log('This session ended with an error:', err.message, triggered);
            }

            // Log some basic stats
            console.log('This session ended with success.', stats.results, '/', stats.students, ' students participated.', triggered);
        });
    });
}

exports.writeAnswer = functions.database.ref('/timeline/{sessionID}/answers/{userID}')
    .onWrite((event) => {
    // Callback for test only

    console.log('User submitted an set of answer for session:', event.params.sessionID, 'userID:', event.params.userID);
    
    var triggered = ['triggered by userID:', event.params.userID].join(' ');

    var db = admin.database();
    var ref = db.ref(['timeline', event.params.sessionID].join('/'));
    ref.once('value', snap => {
        var session = snap.val();

        // Check if exam sesion exists
        if (!session || !_.isObject(session)) {
            console.log('Exam session not found:', event.params.sessionID, triggered);
            return false;
        }

        // Get current time
        var time = new Date().getTime();

        // Check if session.stop exists
        if (!_.isNumber(session.stop)) {
            console.log('Exam session is corrupted:', event.params.sessionID, triggered);
            return false;
        }

        if (session.stop > time) {
            // Check if all finished
            if (_.isObject(session.answers) && Object.keys(session.answers).length == session.students.length) {
                console.log('Correcting session', event.params.sessionID, triggered);
                return finishQuiz(event.params.sessionID, session);
            }

            console.log('This session is not finished yet. Waiting...', event.params.sessionID, triggered);
            return false;
        }
        
        console.log('Correcting session', event.params.sessionID, triggered);
        return finishQuiz(event.params.sessionID, session);
    });

    return true;
});